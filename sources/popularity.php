<?php
$gender_interest = strtolower($my_user->getGenderInterest());

$my_rank = $db->query("SELECT * FROM 
	(SELECT a.*, @row:=@row+1 AS `result` FROM users a, (SELECT @row:=0) s ORDER BY id ASC) a 
	WHERE id = '".$my_user->id."' ORDER BY is_featured DESC");
$my_rank = $my_rank->fetch_object();
$my_rank = $my_rank->result;

$do['page']['name'] = 'Popularity';

include('layout/header.phtml');
include('layout/popularity.phtml');
include('layout/chat/main.phtml');
include('layout/footer.phtml');
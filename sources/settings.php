<?php
$birthday = $my_user->birthday;
$birth_day = date('j',$birthday);
$birth_month = date('F',$birthday);
$birth_year = date('Y',$birthday);
$gender = $my_user->gender;
$languages = scandir('languages');

if(isset($_POST['save_1'])) {
	$name = trim($db->real_escape_string($_POST['name']));
	$gender = $_POST['gender'];
	$birth_day = $_POST['birth_day'];
	$birth_month = $_POST['birth_month'];
	$birth_year = $_POST['birth_year'];
	$birthday = strtotime($birth_month.'/'.$birth_day.'/'.$birth_year);
	$endtime = time();
	$age = date('Y',$endtime) - date('Y',$birthday);
	if(date('z',$endtime) < date('z',$birthday)) {
		$age--;
	}
	$db->query("UPDATE users SET full_name='".$name."',gender='".$gender."',birthday='".$birthday."',age='".$age."' WHERE id='".$my_user->id."'");
	header('Location: '.$system->getDomain().'/settings?success');
}

if(isset($_POST['save_2'])) {
	$email = trim($db->real_escape_string($_POST['email']));
	$new_password = trim($db->real_escape_string($_POST['new_password']));
	$current_password = trim($db->real_escape_string($_POST['current_password']));
	$language = $_POST['language'];
	if(!empty($my_user->email)) {
		// Regular users
		if(!empty($current_password)) {
			if(!empty($new_password)) {
				if($auth->hashPassword($current_password) == $my_user->password) {
					if($new_password == $current_password) {
						$new_password = $auth->hashPassword($new_password);
						$db->query("UPDATE users SET email='".$email."',password='".$new_password."' WHERE id='".$my_user->id."'");
						header('Location: '.$system->getDomain().'/settings?success');
					} else {
						header('Location: '.$system->getDomain().'/settings?error=Password does not match');
					}
				} else {
					header('Location: '.$system->getDomain().'/settings?error=Password does not match');
				}
			} else {
				if($auth->hashPassword($current_password) == $my_user->password) {
					$db->query("UPDATE users SET email='".$email."' WHERE id='".$my_user->id."'");
					header('Location: '.$system->getDomain().'/settings?success');
				} else {
					header('Location: '.$system->getDomain().'/settings?error=Password does not match');
				}
			}
		} else { 
			header('Location: '.$system->getDomain().'/settings?error=Enter your current password');
		}
	} else {
		// Facebook users
		if(!empty($email) && !empty($new_password)) {
			$new_password = $auth->hashPassword($new_password);
			$db->query("UPDATE users SET email='".$email."',password='".$new_password."' WHERE id='".$my_user->id."'");
			header('Location: '.$system->getDomain().'/settings?success');
		} else {
			header('Location: '.$system->getDomain().'/settings?error=Please, fill in all fields');
		}
	}
	$_SESSION['language'] = $language;
}

$do['page']['name'] = 'Settings';

include('layout/header.phtml');
include('layout/settings.phtml');
include('layout/chat/main.phtml');
include('layout/footer.phtml');
<?php
$id = $_GET['id'];

$profile = new User($id);

if(empty($profile->id)) {
	header('Location:'.$system->getDomain().'/people');
	exit;
}

$media = $profile->getPhotos();
$score = $profile->getScore();
$owned_awards = $profile->getAwards();
$owned_gifts = $profile->getGifts();
$verifications = $profile->getVerifications();
$distance = $profile->getDistance($my_user);
$interests = $profile->getInterests();

if(isset($_POST['save_1'])) {
	$education = trim($db->real_escape_string($_POST['education']));
	$work = trim($db->real_escape_string($_POST['work']));
	$db->query("UPDATE users SET education='".$education."',work='".$work."' WHERE id='".$my_user->id."'");
	header('Location: '.$system->getDomain().'/user/'.$my_user->id);
}

if(isset($_POST['save_2'])) {
	$here_to = $_POST['here_to'];
	$db->query("UPDATE users SET here_to='".$here_to."' WHERE id='".$my_user->id."'");
	header('Location: '.$system->getDomain().'/user/'.$my_user->id);
}

if(isset($_POST['save_3'])) {
	$bio = $_POST['bio'];
	$gender = $_POST['gender'];
	$sexuality = $_POST['sexuality'];
	$height = $db->real_escape_string($_POST['height']);
	$weight = $db->real_escape_string($_POST['weight']);
	$relationship = $_POST['relationship'];
	$db->query("UPDATE users SET bio='".$bio."',gender='".$gender."',sexuality='".$sexuality."',height='".$height."',weight='".$weight."',relationship='".$relationship."' WHERE id='".$my_user->id."'");
	header('Location: '.$system->getDomain().'/user/'.$my_user->id);
}

$do['page']['name'] = $profile->full_name.', '.$profile->age;;
$do['menu']['people'] = 'active';

include('layout/header.phtml');
switch ($my_user->id) {
	case $profile->id:
	include('layout/profile/my_profile.phtml');
	break;
	default:
	include('layout/profile/profile.phtml');
	break;
}
include('layout/chat/main.phtml');
include('layout/footer.phtml');
<?php
$filter = $db->query("SELECT * FROM filters WHERE user_id = ".$my_user->id."");
$num_rows = $filter->num_rows;
if($num_rows >= 1) {
	$filter = $filter->fetch_object();
} else {
	$filter = new stdClass();
	$filter->city = $my_user->city;
	$filter->country = $my_user->country;
	$filter->here_to = 1;
	$filter->sexual_preference = 'Male';
	$filter->age_range = '16,100';
	$filter->distance_range = '0,1000';
}

$filter->split_age_range = explode(',',$filter->age_range);
$filter->split_distance_range = explode(',',$filter->distance_range);

if(isset($_POST['filter'])) {
	$location = $_POST['location'];
	$location = explode(',',$location);
	if(count($location) == 2) {
		$city = trim($location[0]);
		$country = trim($location[1]);
	} else {
		$city = trim($location[0]);
		$country = trim($location[2]);
	}
	$sexual_preference = $_POST['sexual_preference'];
	$male = array_search('male',$sexual_preference);
	$female = array_search('female',$sexual_preference);
	if(is_numeric($male) && !is_numeric($female)) {
		$sexual_preference = 'Male';
	} elseif(!is_numeric($male) && is_numeric($female)) {
		$sexual_preference = 'Female';
	} else {
		$sexual_preference = 'All';
	}
	$here_to = $_POST['here_to'];
	$here_to = $here_to[0];
	$age_range = $_POST['age_range'];
	$distance_range = $_POST['distance_range'];
	$exist = $db->query("SELECT id FROM filters WHERE user_id='".$my_user->id."'");
	if($exist->num_rows == 0) {
		$db->query("
			INSERT INTO filters(user_id,here_to,sexual_preference,country,city,age_range,distance_range) 
			VALUES ('".$my_user->id."','".$here_to."','".$sexual_preference."','".$country."','".$city."'
				,'".$age_range."','".$distance_range."')");
	} else {
		$db->query("
			UPDATE filters SET here_to='".$here_to."',sexual_preference='".$sexual_preference."',country='".$country."',city='".$city."'
			,age_range='".$age_range."',distance_range='".$distance_range."' 
			WHERE user_id='".$my_user->id."'");
	}
	header('Location: '.$system->getDomain().'/people');
}

if($filter->sexual_preference == 'All') {
	$gender = "gender IS NOT NULL";
} else {
	$gender = "gender='".$filter->sexual_preference."'";
}

$where = "WHERE here_to='".$filter->here_to."' AND ".$gender." AND country LIKE '".$filter->country."'
AND city LIKE '".$filter->city."' AND (age >= '".$filter->split_age_range[0]."' AND age <= '".$filter->split_age_range[1]."')";

$query = 'SELECT * FROM users '.$where.' AND id != '.$my_user->id.' ORDER BY rise_up_people DESC, id DESC';

$per_page = 8;
$count = $db->query($query)->num_rows;
$last_page = ceil($count/$per_page);
if(isset($_GET['pagination'])) { $p = $_GET['pagination']; } else { $p = 1; }
if($p < 1) { $p = 1; } elseif($p > $last_page) { $p = $last_page; }
$limit = 'LIMIT ' .($p - 1) * $per_page .',' .$per_page;
$query .= " $limit";

$people = $db->query($query);


$do['page']['name'] = 'People';
$do['menu']['people'] = 'active';

include('layout/header.phtml');
include('layout/people.phtml');
include('layout/chat/main.phtml');
include('layout/footer.phtml');
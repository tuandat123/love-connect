<?php
$gender_interest = strtolower($my_user->getGenderInterest());

if($my_user->isPromoted('people')) {
	$is_promoted['people'] = 'disabled';
}  else {
	$is_promoted['people'] = '';
}

if($my_user->isPromoted('encounters')) {
	$is_promoted['encounters'] = 'disabled';
} else {
	$is_promoted['encounters'] = '';
}

if($my_user->isPromoted('spotlight')) {
	$is_promoted['spotlight'] = 'disabled';
} else {
	$is_promoted['spotlight'] = '';
}

$do['page']['name'] = 'Credits';

include('layout/header.phtml');
include('layout/credits.phtml');
include('layout/chat/main.phtml');
include('layout/footer.phtml');
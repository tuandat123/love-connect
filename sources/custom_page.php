<?php
$id = $_GET['id'];
$page = $db->query("SELECT * FROM pages WHERE id='".$id."'");
$page = $page->fetch_object();
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta charset="utf-8">
  <title><?=$system->translate('Meet_New_People')?> <?=$system->settings->website_name?></title>
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-16x16.png" sizes="16x16">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  <meta property="fb:app_id" content="1266543503442629">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?=$system->getDomain()?>">
  <meta property="og:title" content="<?=$system->translate('Meet_New_People')?> <?=$system->settings->website_name?>">
  <meta property="og:description" content="<?=$system->settings->website_description?>">
  <meta property="og:image" content="<?=$system->getDomain()?>/img/logo-medium.png">
  <link href="<?=$system->getDomain()?>/assets/bootstrap3/css/bootstrap.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/css/plugins.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/css/theme.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/landing/landing.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/landing/login.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/fonts/icomoon/styles.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/fonts/themify/styles.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/fonts/linea/styles.css" rel="stylesheet">
  <script>
  var base = '<?=$system->getDomain()?>';
  </script>
</head>
<body>
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1266543503442629',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };
  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
  </script>
  <nav class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?=$system->getDomain()?>/index.php"><img src="<?=$system->getDomain()?>/img/logo.png"></a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-left">
          <li>
            <a href="#" class="language-select">
              <select class="select2">
                <?php
                $lang_dir = scandir('languages');
                foreach($lang_dir as $file) { 
                  if(file_exists('languages/'.$file.'/language.php')) {
                    echo '<option value="'.$file.'">'.ucfirst($file).'</option> ';
                  } 
                }
                ?>
              </select>
            </a>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a class="help"><?=$system->translate('Already_Member')?></a>
          </li>
          <li>
            <a href="<?=$system->getDomain()?>/index.php" class="btn btn-default btn-sm btn-fill"><?=$system->translate('Log_In')?></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="inner-container">
      <div id="page">
        <h1> <?=$page->page_title?> </h1>
        <p> <?=$page->content?> </p>
      </div>
    </div>
  </div>
  <footer>
    <?=$widget->footerPages()?>
</footer>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$system->getDomain()?>/assets/bootstrap3/js/bootstrap.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="<?=$system->getDomain()?>/assets/landing/landing.js" type="text/javascript"></script>
</body>
</html>
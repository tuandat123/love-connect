<?php

header('Content-type: application/json');
session_set_cookie_params(172800);
session_start();
require('core/classes.php');
$system = new Core;
$auth = new Auth;
$geo = new Geo;
$system->getLanguage();
$db = $system->db();

if (isset($_POST['create_account'])) {

    $first_name = $db->real_escape_string(htmlspecialchars($_POST['first_name']));
    $email = $db->real_escape_string(htmlspecialchars($_POST['email']));
    $birth_day = $db->real_escape_string($_POST['birth_day']);
    $birth_month = $db->real_escape_string($_POST['birth_month']);
    $birth_year = $db->real_escape_string($_POST['birth_year']);
    $gender = $db->real_escape_string($_POST['gender']);
    $here_to = $db->real_escape_string($_POST['here_to']);
    $birthday = strtotime($birth_month . '/' . $birth_day . '/' . $birth_year);
    $endtime = time();
    $age = date('Y', $endtime) - date('Y', $birthday);

    if (date('z', $endtime) < date('z', $birthday)) {
        $age--;
    }
    $password = $db->real_escape_string(htmlspecialchars($_POST['password']));

    $user = $db->query("SELECT id FROM users WHERE email='" . $email . "'");
   
    $array_return = ["error_message" => null,"success" => false, "type_message" => null];
    
    if ($user->num_rows >= 1) {
        $array_return['error_message'] = $system->translate('email_exit');
        $array_return['success'] = false;
        $array_return['type_message'] = "email";
    } else {
        
        if (strlen($first_name) > 30) {
            $array_return['error_message'] = $system->translate('name_max_length');
            $array_return['success'] = false;
            $array_return['type_message'] = "name";
            echo json_encode($array_return);
            exit();
        } 
        
        if (strlen($email) > 100) {
            $array_return['error_message'] = $system->translate('email_max_length');
            $array_return['success'] = false;
            $array_return['type_message'] = "email";
            echo json_encode($array_return);
            exit();
        }
        
        if (strlen($password) > 123) {
            $array_return['error_message'] = $system->translate('pass_max_length');
            $array_return['success'] = false;
            $array_return['type_message'] = "pass";
            echo json_encode($array_return);
            exit();
        }
        
        
        $db->query("INSERT INTO users (full_name,email,birthday,password,credits,registered,
			last_login,last_active,profile_picture,gender,here_to,age) VALUES ('" . $first_name . "','" . $email . "','" . $birthday . "',
			'" . $auth->hashPassword($password) . "','0','" . time() . "','" . time() . "','" . time() . "','default_avatar.png','" . $gender . "'
			,'" . $here_to . "','" . $age . "')");

        $user_id = $db->insert_id;

        if (!empty($_POST['latitude']) && !empty($_POST['longitude']) && !empty($_POST['city']) && !empty($_POST['country'])) {
            
            $reverse = $geo->reverseGeo();
            $latitude = $_POST['latitude'];
            $longitude = $_POST['longitude'];
            $city =  str_replace('"', '', $_POST['city']);
            $country =  str_replace('"', '', $_POST['country']);
            $ip = $_SERVER['REMOTE_ADDR'];
            
            $db->query("UPDATE users SET latitude='" . $latitude . "',longitude='" . $longitude . "',
				city='" . $city . "',country='" . $country . "',ip='" . $ip . "' WHERE id='" . $user_id . "'");
        } else {
            $reverse = $geo->reverseGeo();
            $latitude = $reverse->latitude;
            $longitude = $reverse->longitude;
            $city = $reverse->city;
            $country = $reverse->country_name;
            $ip = $_SERVER['REMOTE_ADDR'];
            $db->query("UPDATE users SET latitude='" . $latitude . "',longitude='" . $longitude . "',
				city='" . $city . "',country='" . $country . "',ip='" . $ip . "' WHERE id='" . $user_id . "'");
        }
        
        $age_range = "16,100";
        $distance_range = "0,1000";
        $sexual_preference = "All";
      
        $db->query("INSERT INTO filters(user_id,here_to,sexual_preference,country,city,age_range,distance_range) 
			VALUES ('".$user_id."','".$here_to."','".$sexual_preference."','".$country."','".$city."'
				,'".$age_range."','".$distance_range."')");
        
        $_SESSION['auth'] = true;
        $_SESSION['email'] = $email;
        $_SESSION['user_id'] = $user_id;
        $_SESSION['full_name'] = $first_name;
        $_SESSION['is_admin'] = 0;
        
        $array_return['error_message'] = null;
        $array_return['success'] = true;
        $array_return['type_message'] = null;
    }
    
    echo json_encode($array_return);
   
}
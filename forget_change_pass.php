<?php

header('Content-type: application/json');
session_set_cookie_params(172800);
session_start();
require('core/classes.php');

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require 'vendor/autoload.php';

$system = new Core;
$auth = new Auth;
$geo = new Geo;
$endcodeMail = new EncodeMail;

$system->getLanguage();
$db = $system->db();

try {
    if (isset($_POST['forgot_pass'])) {

        $email = $db->real_escape_string(htmlspecialchars($_POST['email']));

        $user = $db->query("SELECT id FROM users WHERE email='" . $email . "'");

        $array_return = ["error_message" => null, "success" => false, "type_message" => null];

        if ($user->num_rows == 0) {
            $array_return['error_message'] = $system->translate('Email_not_exits');
            $array_return['success'] = false;
            $array_return['type_message'] = "email";
        } else {

            if (strlen($email) > 100) {
                $array_return['error_message'] = $system->translate('email_max_length');
                $array_return['success'] = false;
                $array_return['type_message'] = "email";
                echo json_encode($array_return);
                exit();
            }

            $mail = new PHPMailer();

            // Gọi đến lớp SMTP
            $mail->IsSMTP();

            $mail->SMTPDebug = 0;

            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'smtp.mailtrap.io';
            $mail->Port = 2525;
            $mail->Username = 'a8511c73807fc8';
            $mail->Password = '46da81ceef6d14';

            // Thiết lập thông tin người gửi và email người gửi
            $mail->SetFrom('datpdt@rikkeisoft.com', 'admin');

            // Thiết lập thông tin người nhận và email người nhận
            $mail->AddAddress('e1bd2fec37-cf18cf@inbox.mailtrap.io', '');

            // Thiết lập email reply
            $mail->AddReplyTo('e1bd2fec37-cf18cf@inbox.mailtrap.io');

            // Thiết lập tiêu đề
            $mail->Subject = $system->translate('Title_mail');

            // Thiết lập charset
            $mail->CharSet = 'utf-8';

            // Thiết lập nội dung

            $filename = "temple_mail_forgot.php";
            $contentFile = file_get_contents($filename);
            
            $urlLogo = $system->getDomain()."/img/logo-color.png";
            $urlGooglePlay = $system->getDomain()."/img/google-play.png";
            $urlFaceBook = $system->getDomain()."/img/icon-facebook.png";
            
            define("ENCRYPTION_KEY", "!@#$%^&*");
            
            // Get date time now
            $dateTimeNow = date("YmdHis");

            // Create the string needs coding
            $coding = trim($email . '_' . $dateTimeNow, " ");
            
            $encryptedCoding = $endcodeMail->encrypt($coding, ENCRYPTION_KEY);
            
            // Create URL for send Email
            $url = $system->getDomain() . '/forgot/password?k=' . $encryptedCoding;
          
            // Replace
            $contentReplace = str_replace(
                    ['{title_mail}', '{Button_Temple_Mail}', '{Description_Temple_Mail}', '{Dowload_App_Google_Play}', '{Follow_FaceBook}',
                '{Url_Image_Logo}','{Url_Image_Google_Play}','{Url_Image_FaceBook}','{Link_Url_Reset_Pass}'
                    ], [
                $system->translate('Title_Temple_Mail'), $system->translate('Button_Temple_Mail'),
                $system->translate('Description_Temple_Mail'), $system->translate('Dowload_App_Google_Play'),
                $system->translate('Follow_FaceBook'),$urlLogo,$urlGooglePlay,$urlFaceBook,$url
                    ], $contentFile
            );
            
            $body = $contentReplace;

            //$mail->Body = $body;
            $mail->MsgHTML($body);

            if ($mail->Send()) {
                $array_return['error_message'] = null;
                $array_return['success'] = true;
                $array_return['type_message'] = $system->translate('Send_Mail_Forget_Success');
            } else {
                $array_return['error_message'] = null;
                $array_return['success'] = true;
                $array_return['type_message'] = "mail send fail";
            }
        }

        echo json_encode($array_return);
    }
} catch (Exception $ex) {
    echo 'Message could not be sent. Mailer Error: ' . $ex;
    exit();
}


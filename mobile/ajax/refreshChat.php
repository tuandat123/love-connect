<?php
session_set_cookie_params(172800);
session_start();
require('../../core/classes.php');
$system = new Core;
$auth = new Auth;
$helper = new Helper;
$widget = new Widget;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

$user2 = $_GET['receiver_id'];

$messages = $db->query("SELECT * FROM messages WHERE (user1='".$my_user->id."' AND user2='".$user2."') OR (user1='".$user2."' AND user2='".$my_user->id."') ORDER BY id ASC");

while($message = $messages->fetch_object()) {
	$sender = new User($message->user1);
	if($message->is_sticker == 1) {
		$sticker = $db->query("SELECT id,pack_id,path FROM stickers WHERE id='".$message->sticker_id."'");
		$sticker = $sticker->fetch_object();
		if($sender->id == $my_user->id) {
			echo '
			<li class="list-chat right">
			<div class="w-clearfix column-right chat right">
			<div class="arrow-globe right"></div>
			<div class="chat-text right"><img src="'.$system->getDomain().'/img/stickers/'.$sticker->pack_id.'/'.$sticker->path.'" class="sticker-chat"></div>
			<div class="chat-time right">'.$helper->timeAgo($message->time).'</div>
			</div>
			</li>
			';
		} else {
			echo '
			<li class="w-clearfix list-chat">
			<div class="column-left chat">
			<div class="image-message chat"><img src="'.$system->getDomain().'/uploads/'.$sender->profile_picture.'">
			</div>
			</div>
			<div class="w-clearfix column-right chat">
			<div class="arrow-globe"></div>
			<div class="chat-text"><img src="'.$system->getDomain().'/img/stickers/'.$sticker->pack_id.'/'.$sticker->path.'" class="sticker-chat"></div>
			<div class="chat-time">'.$helper->timeAgo($message->time).'</div>
			</div>
			</li>
			';
		} 
	} else {
		if($sender->id == $my_user->id) {
			echo '
			<li class="list-chat right">
			<div class="w-clearfix column-right chat right">
			<div class="arrow-globe right"></div>
			<div class="chat-text right">'.$message->message.'</div>
			<div class="chat-time right">'.$helper->timeAgo($message->time).'</div>
			</div>
			</li>
			';
		} else {
			echo '
			<li class="w-clearfix list-chat">
			<div class="column-left chat">
			<div class="image-message chat"><img src="'.$system->getDomain().'/uploads/'.$sender->profile_picture.'">
			</div>
			</div>
			<div class="w-clearfix column-right chat">
			<div class="arrow-globe"></div>
			<div class="chat-text">'.$message->message.'</div>
			<div class="chat-time">'.$helper->timeAgo($message->time).'</div>
			</div>
			</li>
			';
		}
	}
}
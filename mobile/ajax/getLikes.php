<?php
session_set_cookie_params(172800);
session_start();
require('../../core/classes.php');
$system = new Core;
$widget = new Widget;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

$likes = $db->query("
	SELECT * FROM profile_likes 
	WHERE profile_id='".$my_user->id."' 
	AND viewer_id != '".$my_user->id."' 
	ORDER BY id DESC 
	LIMIT 20");

if($likes->num_rows >= 1) {
while($like = $likes->fetch_object()) { 
	$profile = new User($like->viewer_id);
	echo '
	<div class="visitor">
	<a href="user.php?id='.$profile->id.'" data-load="1">
	<img src="'.$system->getDomain().'/uploads/'.$profile->profile_picture.'" class="visitor-photo">
	<div class="visitor-info">
	'.$profile->full_name.'
	</div>
	</a>
	</div>
	';
}
} else {
	echo '
	<div class="page-error">
	<img src="'.$system->getDomain().'/img/icons/heart.png">
	<h4> No new likes </h4>
	<p> No new users have liked you, <br> please try again later </p>
	</div>
	';
}
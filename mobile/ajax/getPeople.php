<?php
session_set_cookie_params(172800);
session_start();
require('../../core/classes.php');
$system = new Core;
$auth = new Auth;
$widget = new Widget;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

$filter = $db->prepare("SELECT * FROM filters WHERE user_id = ?");
$filter->bind_param('s', $my_user->id);
$filter->execute();
$filter = $filter->get_result();
$num_rows = $filter->num_rows;
if($num_rows >= 1) {
  $filter = $filter->fetch_object();
} else {
  $filter = new stdClass();
  $filter->city = $my_user->city;
  $filter->country = $my_user->country;
  $filter->here_to = 1;
  $filter->sexual_preference = 'Male';
  $filter->age_range = '16,100';
  $filter->distance_range = '0,1000';
}

$filter->split_age_range = explode(',',$filter->age_range);
$filter->split_distance_range = explode(',',$filter->distance_range);

if(isset($_POST['filter'])) {
  $location = $_POST['location'];
  $location = explode(',',$location);
  if(count($location) == 2) {
    $city = trim($location[0]);
    $country = trim($location[1]);
  } else {
    $city = trim($location[0]);
    $country = trim($location[2]);
  }
  $sexual_preference = $_POST['sexual_preference'];
  $male = array_search('male',$sexual_preference);
  $female = array_search('female',$sexual_preference);
  if(is_numeric($male) && !is_numeric($female)) {
    $sexual_preference = 'Male';
  } elseif(!is_numeric($male) && is_numeric($female)) {
    $sexual_preference = 'Female';
  } else {
    $sexual_preference = 'All';
  }
  $here_to = $_POST['here_to'];
  $here_to = $here_to[0];
  $age_range = $_POST['age_range'];
  $distance_range = $_POST['distance_range'];
  $exist = $db->query("SELECT id FROM filters WHERE user_id='".$my_user->id."'");
  if($exist->num_rows == 0) {
    $db->query("
      INSERT INTO filters(user_id,here_to,sexual_preference,country,city,age_range,distance_range) 
      VALUES ('".$my_user->id."','".$here_to."','".$sexual_preference."','".$country."','".$city."'
        ,'".$age_range."','".$distance_range."')");
  } else {
    $db->query("
      UPDATE filters SET here_to='".$here_to."',sexual_preference='".$sexual_preference."',country='".$country."',city='".$city."'
      ,age_range='".$age_range."',distance_range='".$distance_range."' 
      WHERE user_id='".$my_user->id."'");
  }
  header('Location: '.$system->getDomain().'/people');
}

if($filter->sexual_preference == 'All') {
  $gender = "gender IS NOT NULL";
} else {
  $gender = "gender='".$filter->sexual_preference."'";
}

$where = "WHERE here_to='".$filter->here_to."' AND ".$gender." AND country='".$filter->country."'
AND city='".$filter->city."' AND (age >= '".$filter->split_age_range[0]."' AND age <= '".$filter->split_age_range[1]."')";

$query = 'SELECT * FROM users '.$where.' AND id != '.$my_user->id.' ORDER BY rise_up_people DESC, id DESC';

$people = $db->query($query);

if($people->num_rows >= 1) {
  while($profile = $people->fetch_object()) {
    $profile = new User($profile->id);
    echo '
    <div class="items-3">
    <a href="user.php?id='.$profile->id.'"> 
    <div class="people-list-item">
    <div class="people-list-avatar" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.7)),url(\''.$system->getDomain().'/uploads/'.$profile->profile_picture.'\')"></div>
    <div class="people-list-info">
    '; 
    if($profile->isOnline()) { 
      echo '<i class="online-status online"></i>';
    } else { 
      echo '<i class="online-status offline"></i>'; 
    }
    echo '
    '.$profile->full_name.', '.$profile->age.'
    </div>
    </div>
    </a>
    </div>
    ';
  }
}
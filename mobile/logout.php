<?php
session_set_cookie_params(172800);
session_start();
session_destroy();
header('Location: index.php');
exit;
<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$widget = new Widget;
$system->getLanguage();
$db = $system->db();

if(!$auth->isLogged()) {
  header('Location: '.$system->getDomain().'/mobile/login.php');
  exit;
} else {
  $my_user = new User($_SESSION['user_id']);
  $my_user->markActivity();
}

$id = $_GET['id'];

$profile = new User($id);

if(empty($profile->id)) {
  header('Location: encounters.php');
  exit;
}

$media = $profile->getPhotos();
$owned_gifts = $profile->getGifts();
$interests = $profile->getInterests();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Datoo Mobile</title>
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-16x16.png" sizes="16x16">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/framework.css">
  <link rel="stylesheet" type="text/css" href="css/theme.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/swipebox.min.css">
  <script src="js/webfont.js"></script>
  <script>
  var base = '<?=$system->getDomain()?>/mobile';
  var page = 'profile';
  </script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <section class="w-section mobile-wrapper">
    <div class="page-content" id="main-stack" data-scroll="0">
      <div class="w-nav navbar" data-collapse="all" data-animation="over-left" data-duration="400" data-contain="1" data-no-scroll="1" data-easing="ease-out-quint">
        <div class="w-container">
          <nav class="w-nav-menu nav-menu" role="navigation">
            <div class="nav-menu-header">
              <div class="sidebar-user-area">
                <a href="user.php?id=<?=$my_user->id?>" data-load="1">
                  <img src="<?=$system->getDomain()?>/uploads/<?=$my_user->profile_picture?>" class="sidebar-user-photo">
                </a>
                <h4 class="sidebar-user-name"> <?=$my_user->full_name?> </h4>
              </div>
            </div>
            <a class="w-clearfix w-inline-block nav-menu-link" href="encounters.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-ios-photos"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Encounters')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="people.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-person-stalker"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('People')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="messages.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-chatboxes"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Messages')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="visitors.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-eye"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Profile_Visitors')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="likes.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-heart"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Profile_Likes')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="settings.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-ios-gear"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Settings')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link last" href="logout.php" data-load="0">
              <div class="icon-list-menu">
                <div class="ion-android-exit"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Logout')?></div>
            </a>
            <div class="separator-bottom"></div>
            <div class="separator-bottom"></div>
            <div class="separator-bottom"></div>
          </nav>
          <div class="wrapper-mask" data-ix="menu-mask"></div>
          <div class="navbar-title"><?=$profile->full_name?></div>
          <div class="w-nav-button navbar-button left" id="menu-button" data-ix="hide-navbar-icons">
            <div class="navbar-button-icon home-icon">
              <div class="bar-home-icon"></div>
              <div class="bar-home-icon"></div>
              <div class="bar-home-icon"></div>
            </div>
          </div>
          <?php if($profile->id != $my_user->id) { ?>
          <a href="chat.php?id=<?=$profile->id?>&receiver=<?=$profile->full_name?>" class="w-inline-block navbar-button right" data-load="1" style="z-index:-1;">
            <div class="navbar-button-icon icon message-user">
              <div class="ion-ios-chatbubble"></div>
            </div>
          </a>
          <? } ?>
        </div>
      </div>
      <div class="body">
        <div class="profile-top">
          <img class="profile-photo" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.7)),url('<?=$system->getDomain()?>/uploads/<?=$profile->profile_picture?>')">
          <div class="profile-info">
            <?php 
            if($profile->isOnline()) { 
              echo '<i class="online-status online"></i>'; 
            } else { 
              echo '<i class="online-status offline"></i>'; 
            } 
            ?>
            <?=$profile->full_name?>, <?=$profile->age?>
          </div>
        </div>
        <div class="profile-content">
          <div class="profile-section">
            <h2> <?=$system->translate('Location')?> </h2>
            <p> 
              <?=$profile->city?> <?=$profile->country?> 
            </p>
          </div>
          <div class="profile-section">
            <h2> <?=$interests->num_rows?> <?=$system->translate('Interests')?> </h2>
            <?php if($interests->num_rows >= 1) { ?>
            <?php while($interest = $interests->fetch_object()) { ?>
            <div class="interest-item"><?=htmlentities($interest->name)?></div>
            <?
          }
        } else { 
          echo $system->translate('Nothing_To_Show');
        } 
        ?>
      </div>
      <div class="profile-section">
        <h2> <?=$media->num_rows?> Photos </h2>
        <ul class="profile-gallery">
          <?php
          if($media->num_rows >= 1) {
            while($photo = $media->fetch_object()) {
              ?>
              <li>
                <a rel="gallery-1" href="<?=$system->getDomain()?>/uploads/<?=$photo->path?>" class="swipebox">
                  <img src="<?=$system->getDomain()?>/uploads/<?=$photo->path?>" style="height:70px;width:70px;border-radius:5px;"/>
                </a>
              </li>
              <?php
            }
          } else {
            echo $system->translate('Nothing_To_Show');
          }
          ?>
        </ul>
      </div>
      <div class="profile-section">
        <h2> <?=$system->translate('Personal_Info')?> </h2>
        <div style="padding-bottom:5px;"> <?=htmlentities($profile->bio)?> </div>
        <table class="table table-border" style="width:100%;text-align:center;">
          <tr>
            <td align="left">
              <b style="font-weight:500;"><?=$system->translate('Gender')?></b>
            </td>
            <td>
              <?=htmlentities($system->translate($profile->gender))?>
            </td>
          </tr>
          <tr>
            <td align="left">
              <b style="font-weight:500;"><?=$system->translate('Sexual_Orientation')?></b>
            </td>
            <td>
              <?=htmlentities($profile->sexuality)?>
            </td>
          </tr>
          <tr>
            <td align="left">
              <b style="font-weight:500;"><?=$system->translate('Height')?></b>
            </td>
            <td>
              <?=htmlentities($profile->height)?>
            </td>
          </tr>
          <tr>
            <td align="left">
              <b style="font-weight:500;"><?=$system->translate('Weight')?></b>
            </td>
            <td>
              <?=htmlentities($profile->weight)?>
            </td>
          </tr>
        </table>
      </div>
      <div class="profile-section">
        <h2> <?=$system->translate('Gifts')?> </h2>
        <?php if($owned_gifts->num_rows >= 1) { ?>
        <?php while($owned_gift = $owned_gifts->fetch_object()) { ?>
        <a href="user.php?id=<?=$owned_gift->user1?>">
          <img src="<?=$system->getDomain()?>/img/gifts/<?=$owned_gift->path?>" style="height:50px;width:50px;">
        </a>
        <? 
      } 
    } else { 
      echo $system->translate('Nothing_To_Show');
    } 
    ?>
  </div>
</div>
</div>
</div>
<div class="page-content loading-mask" id="new-stack">
  <div class="loading-icon">
    <div class="navbar-button-icon icon ion-load-d"></div>
  </div>
</div>
</section>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/framework.js"></script>
<script type="text/javascript" src="js/bxslider.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/mobile.js"></script>
<script type="text/javascript" src="js/jquery.swipebox.min.js"></script>
<!--[if lte IE 9]><script src="js/placeholders.min.js"></script><![endif]-->
</body>
</html>
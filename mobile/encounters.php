<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$widget = new Widget;
$system->getLanguage();
$db = $system->db();

if(!$auth->isLogged()) {
  header('Location: '.$system->getDomain().'/mobile/login.php');
  exit;
} else {
  $my_user = new User($_SESSION['user_id']);
  $my_user->markActivity();
}

$count = $db->prepare("SELECT COUNT(id) FROM users WHERE id != ?");
$count->bind_param('s', $my_user->id);
$count->execute();
$count = $count->get_result();

if($count->num_rows >= 1) { 
  switch($my_user->last_encounter) {
    case 0:
    $encounters = $db->prepare("SELECT * FROM users WHERE id != ? ORDER BY RAND()");
    $encounters->bind_param('s', $my_user->id);
    break;
    default:
    $encounters = $db->prepare("SELECT * FROM users WHERE id = ?");
    $encounters->bind_param('s', $my_user->last_encounter);
    break;
  }
}

$encounters->execute();
$encounters = $encounters->get_result();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Datoo Mobile</title>
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-16x16.png" sizes="16x16">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/framework.css">
  <link rel="stylesheet" type="text/css" href="css/theme.css">
  <link rel="stylesheet" type="text/css" href="css/lightslider.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="js/webfont.js"></script>
  <script>
  var base = '<?=$system->getDomain()?>/mobile';
  var page = 'encounters';
  </script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <section class="w-section mobile-wrapper">
    <div class="page-content" id="main-stack" data-scroll="0">
      <div class="w-nav navbar" data-collapse="all" data-animation="over-left" data-duration="400" data-contain="1" data-no-scroll="1" data-easing="ease-out-quint">
        <div class="w-container">
          <nav class="w-nav-menu nav-menu" role="navigation">
            <div class="nav-menu-header">
              <div class="sidebar-user-area">
                <a href="user.php?id=<?=$my_user->id?>" data-load="1">
                  <img src="<?=$system->getDomain()?>/uploads/<?=$my_user->profile_picture?>" class="sidebar-user-photo">
                </a>
                <h4 class="sidebar-user-name"> <?=$my_user->full_name?> </h4>
              </div>
            </div>
            <a class="w-clearfix w-inline-block nav-menu-link" href="encounters.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-ios-photos"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Encounters')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="people.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-person-stalker"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('People')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="messages.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-chatboxes"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Messages')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="visitors.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-eye"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Profile_Visitors')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="likes.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-heart"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Profile_Likes')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="settings.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-ios-gear"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Settings')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link last" href="logout.php" data-load="0">
              <div class="icon-list-menu">
                <div class="ion-android-exit"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Logout')?></div>
            </a>
            <div class="separator-bottom"></div>
            <div class="separator-bottom"></div>
            <div class="separator-bottom"></div>
          </nav>
          <div class="wrapper-mask" data-ix="menu-mask"></div>
          <?php
          if($encounters->num_rows >= 1) {
            $encounter = $encounters->fetch_object();
            $encounter = new User($encounter->id);
            $media = $encounter->getPhotos();
            $score = $encounter->getScore();
            $distance = $my_user->getDistance($encounter);
            $interests = $encounter->getInterests();
            $my_user->markEncounter($encounter->id);
            ?>
            <div class="navbar-title">
              <a href="user.php?id=<?=$encounter->id?>">
                <?=$encounter->full_name?>
              </a>
            </div>
            <div class="w-nav-button navbar-button left" id="menu-button" data-ix="hide-navbar-icons">
              <div class="navbar-button-icon home-icon">
                <div class="bar-home-icon"></div>
                <div class="bar-home-icon"></div>
                <div class="bar-home-icon"></div>
              </div>
            </div>
            <div class="navbar-button right filter-button" id="filter-button">
              <a href="filter.php" data-load="1">
                <div class="navbar-button-icon">
                  <div class="ion-ios-settings"></div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="body">
          <div class="encounters">
            <ul id="encounterGallery">
              <?php
              if($media->num_rows >= 1) {
                while($photo = $media->fetch_object()) {
                  $full_path = $system->getDomain().'/uploads/'.$photo->path;
                  echo '
                  <li data-thumb="'.$full_path.'" data-src="'.$full_path.'" data-path="'.$photo->path.'">
                  <img src="'.$full_path.'">
                  </li>
                  ';
                }
              } else {
                $full_path = $system->getDomain().'/uploads/'.$encounter->profile_picture;
                echo '
                <li data-thumb="'.$full_path.'" data-src="'.$full_path.'" data-path="'.$encounter->profile_picture.'">
                <img src="'.$full_path.'">
                </li>
                ';
              }
              ?>
            </ul>
            <div class="encounters-controls"> 
              <div id="heart-<?=$encounter->id?>" onclick="likeProfile(<?=$encounter->id?>,true)" style="display:inline;">
                <?php 
                $check = $db->query("SELECT id FROM profile_likes WHERE viewer_id='".$my_user->id."' AND profile_id='".$encounter->id."' LIMIT 1"); ?>
                <?php if($check->num_rows == 0) { 
                  echo '<button class="w-button action-button encounter-button btn-like"><i class="ion-heart"></i></button>';
                } else { 
                  echo '<button class="w-button action-button encounter-button btn-like"><i class="ion-heart"></i></button>';
                } 
                ?>
              </div>
              <div id="times-<?=$encounter->id?>" onclick="dislikeProfile(<?=$encounter->id?>,true)" style="display:inline;">
                <?php 
                $check = $db->query("SELECT id FROM profile_dislikes WHERE viewer_id='".$my_user->id."' AND profile_id='".$encounter->id."' LIMIT 1"); ?>
                <?php if($check->num_rows == 0) { 
                  echo '<button class="w-button action-button encounter-button btn-dislike"><i class="ion-close"></i></button>';
                } else {
                  echo '<button class="w-button action-button encounter-button btn-dislike"><i class="ion-close"></i></button>';
                }
                ?>
              </div>
            </div>
            <div class="encounter-section">
              <h5 class="encounter-sub-title"> Location </h5>
              <p> 
                <?=$encounter->city?> <?=$encounter->country?>
                <?=$distance?>
              </p>
            </div>
            <div class="encounter-section">
              <h5 class="encounter-sub-title"><?=$interests->num_rows?> Interests</h5>
              <?php if($interests->num_rows >= 1) { ?>
              <?php while($interest = $interests->fetch_object()) { ?>
              <div class="interest-item"><?=htmlentities($interest->name)?></div>
              <?
            }
          } else { 
            echo $system->translate('Nothing_To_Show');
          } 
          ?>
        </div>
      </div>
      <? } else { ?>
      <div class="navbar-title">
        <a href="user.php?id=<?=$encounter->id?>">
          <?=$encounter->full_name?>
        </a>
      </div>
      <div class="w-nav-button navbar-button left" id="menu-button" data-ix="hide-navbar-icons">
        <div class="navbar-button-icon home-icon">
          <div class="bar-home-icon"></div>
          <div class="bar-home-icon"></div>
          <div class="bar-home-icon"></div>
        </div>
      </div>
      <div class="navbar-button right filter-button" id="filter-button">
        <a href="filter.php" data-load="1">
          <div class="navbar-button-icon">
            <div class="ion-ios-settings"></div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="page-error">
    <img src="<?=$system->getDomain()?>/img/icons/user-minus.png">
    <h4> No new users </h4>
    <p> You have rated all existing users, <br> please try again later </p>
  </div>
  <?php } ?>
</div>
</div>
</div>
<div class="page-content loading-mask" id="new-stack">
  <div class="loading-icon">
    <div class="navbar-button-icon icon ion-load-d"></div>
  </div>
</div>
</section>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/framework.js"></script>
<script type="text/javascript" src="js/lightslider.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/mobile.js"></script>
<!--[if lte IE 9]><script src="js/placeholders.min.js"></script><![endif]-->
</body>
</html>
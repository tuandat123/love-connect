<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$widget = new Widget;
$system->getLanguage();
$db = $system->db();

if(!$auth->isLogged()) {
  header('Location: '.$system->getDomain().'/mobile/login.php');
  exit;
} else {
  $my_user = new User($_SESSION['user_id']);
  $my_user->markActivity();
}

if(isset($_POST['save_1'])) {

  $full_name = $_POST['full_name'];
  $gender = $_POST['gender'][0];
  $city = $_POST['city'];
  $bio = $db->real_escape_string($_POST['bio']);
  $new_pasword = $_POST['new_pasword'];
  $confirm_new_password = $_POST['confirm_new_password'];
  $sexual_interest = $_POST['sexual_orientation'];

  $db->query("
    UPDATE users SET 
    full_name='".$full_name."',
    gender='".$gender."',
    bio='".$bio."',
    city='".$city."',
    country='".$country."',
    sexual_interest='".$sexual_interest."',
    updated_preferences='1'
    WHERE id='".$my_user->id."'");

  header('Location: settings.php');
  exit;
}

if(isset($_POST['save_2'])) {

  $email = $_POST['email'];
  $language = $_POST['language'];

  $db->query("
    UPDATE users SET 
    email='".$email."',
    language='".$language."',
    WHERE id='".$my_user->id."'");

  $_SESSION['language'] = $language;

  if(!empty($_POST['new_password']) && !empty($_POST['confirm_new_password'])) { 

    if($new_password === $confirm_new_password) {
      $new_password = $auth->hashPassword($new_password);
      $db->query("
        UPDATE users SET 
        password='".$new_password."'
        WHERE id='".$my_user->id."'");
    }
  }
  header('Location: settings.php');
  exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Datoo Mobile</title>
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-16x16.png" sizes="16x16">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/framework.css">
  <link rel="stylesheet" type="text/css" href="css/theme.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/swipebox.min.css">
  <script src="js/webfont.js"></script>
  <script>
  var base = '<?=$system->getDomain()?>/mobile';
  var page = 'messages';
  </script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <section class="w-section mobile-wrapper">
    <div class="page-content" id="main-stack" data-scroll="0">
      <div class="w-nav navbar" data-collapse="all" data-animation="over-left" data-duration="400" data-contain="1" data-no-scroll="1" data-easing="ease-out-quint">
        <div class="w-container">
          <nav class="w-nav-menu nav-menu" role="navigation">
            <div class="nav-menu-header">
              <div class="sidebar-user-area">
                <a href="user.php?id=<?=$my_user->id?>" data-load="1">
                  <img src="<?=$system->getDomain()?>/uploads/<?=$my_user->profile_picture?>" class="sidebar-user-photo">
                </a>
                <h4 class="sidebar-user-name"> <?=$my_user->full_name?> </h4>
              </div>
            </div>
            <a class="w-clearfix w-inline-block nav-menu-link" href="encounters.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-ios-photos"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Encounters')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="people.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-person-stalker"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('People')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="messages.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-chatboxes"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Messages')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="visitors.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-eye"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Profile_Visitors')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="likes.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-heart"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Profile_Likes')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link" href="settings.php" data-load="1">
              <div class="icon-list-menu">
                <div class="icon ion-ios-gear"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Settings')?></div>
            </a>
            <a class="w-clearfix w-inline-block nav-menu-link last" href="logout.php" data-load="0">
              <div class="icon-list-menu">
                <div class="ion-android-exit"></div>
              </div>
              <div class="nav-menu-titles"><?=$system->translate('Logout')?></div>
            </a>
            <div class="separator-bottom"></div>
            <div class="separator-bottom"></div>
            <div class="separator-bottom"></div>
          </nav>
          <div class="wrapper-mask" data-ix="menu-mask"></div>
          <div class="navbar-title"><?=$system->translate('Settings')?></div>
          <div class="w-nav-button navbar-button left" id="menu-button" data-ix="hide-navbar-icons">
            <div class="navbar-button-icon home-icon">
              <div class="bar-home-icon"></div>
              <div class="bar-home-icon"></div>
              <div class="bar-home-icon"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="body no-padding">
        <div class="grey-header">
          <h2 class="grey-heading-title">Profile</h2>
        </div>
        <div class="text-new no-borders">
          <div class="w-form settings-form">
            <form action="" method="post">
              <div>
                <label class="label-form"><?=strtoupper($system->translate('Name'))?></label>
                <input class="w-input input-form" type="text" name="full_name" required="required" value="<?=$my_user->full_name?>">
                <div class="separator-fields"></div>
              </div>
              <div>
                <label class="label-form"><?=strtoupper($system->translate('City'))?></label>
                <input class="w-input input-form city-autocomplete" type="text" name="city" required="required" value="<?=$my_user->city?>" autocomplete="off">
                <div class="separator-fields"></div>
              </div>
              <div>
                <div class="w-clearfix input-form one-line">
                  <label class="label-form middle"><?=strtoupper($system->translate('Gender'))?></label>
                  <div class="w-clearfix radios-container">
                    <div class="w-radio w-clearfix radio-button">
                      <div class="radio-bullet-replacement <?php if($my_user->gender === 'Female') { echo 'checked'; } ?>"></div>
                      <input class="w-radio-input radio-bullet" type="radio" name="gender[]" value="Female">
                      <label class="w-form-label"><?=$system->translate('Female')?></label>
                    </div>
                    <div class="w-radio w-clearfix radio-button">
                      <div class="radio-bullet-replacement <?php if($my_user->gender === 'Male') { echo 'checked'; } ?>"></div>
                      <input class="w-radio-input radio-bullet" type="radio" name="gender[]" value="Male">
                      <label class="w-form-label"><?=$system->translate('Male')?></label>
                    </div>
                  </div>
                </div>
                <div class="separator-fields"></div>
              </div>
              <div>
                <label class="label-form"><?=strtoupper($system->translate('Sexual_Orientation'))?></label>
                <select class="w-input input-form" name="sexual_orientation" required="required">
                  <option value="1" <?php if($my_user->sexuality == 'Straight') { echo 'selected'; } ?>> Straight </option>
                  <option value="2" <?php if($my_user->sexuality == 'Gay') { echo 'selected'; } ?>> Gay </option>
                  <option value="3" <?php if($my_user->sexuality == 'Lesbian') { echo 'selected'; } ?>> Lesbian </option>
                  <option value="4" <?php if($my_user->sexuality == 'Bisexual') { echo 'selected'; } ?>> Bisexual </option>
                </select>
                <div class="separator-fields"></div>
              </div>
              <div>
                <label class="label-form"><?=strtoupper($system->translate('Self-summary'))?></label>
                <textarea class="w-input input-form" name="bio" required="required"><?=$my_user->bio?></textarea>
                <div class="separator-fields"></div>
              </div>
              <input class="w-button action-button" type="submit" name="save_1" value="<?=$system->translate('Save')?>" data-wait="Please wait...">
            </form>
            <div class="w-form-done">
              <p>Thank you! Your submission has been received!</p>
            </div>
            <div class="w-form-fail">
              <p>Oops! Something went wrong while submitting the form</p>
            </div>
          </div>
        </div>
        <div class="grey-header">
          <h2 class="grey-heading-title">Account</h2>
        </div>
        <div class="text-new no-borders">
          <div class="w-form settings-form">
            <form action="" method="post">
              <div>
                <label class="label-form"><?=strtoupper($system->translate('Email'))?></label>
                <input class="w-input input-form" type="email" name="email" required="required" value="<?=$my_user->email?>">
                <div class="separator-fields"></div>
              </div>
              <div>
                <label class="label-form"><?=strtoupper($system->translate('New_Password'))?></label>
                <input class="w-input input-form" type="password" name="new_password">
                <div class="separator-fields"></div>
              </div>
              <div>
                <label class="label-form"><?=strtoupper($system->translate('Confirm_Password'))?></label>
                <input class="w-input input-form" type="password" name="confirm_new_password">
                <div class="separator-fields"></div>
              </div>
              <div>
                <label class="label-form"><?=strtoupper($system->translate('Language'))?></label>
                <select class="w-input input-form" name="language">
                 <?php
                 $lang_dir = scandir('../languages');
                 foreach($lang_dir as $file) { 
                  if(file_exists('../languages/'.$file.'/language.php')) {
                    if($my_user->language == $file) {
                      echo '<option value="'.$file.'" selected>'.ucfirst($file).'</option>';
                    } else {
                      echo '<option value="'.$file.'">'.ucfirst($file).'</option>';
                    }
                  } 
                }
                ?>
              </select>
              <div class="separator-fields"></div>
            </div>
            <input class="w-button action-button" type="submit" name="save_2" value="<?=$system->translate('Save')?>" data-wait="Please wait...">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-content loading-mask" id="new-stack">
  <div class="loading-icon">
    <div class="navbar-button-icon icon ion-load-d"></div>
  </div>
</div>
</section>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/framework.js"></script>
<script src="<?=$system->getDomain()?>/assets/js/autocomplete.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<!--[if lte IE 9]><script src="js/placeholders.min.js"></script><![endif]-->
</body>
</html>
<?php
require('../core/classes.php');
$system = new Core;
$db = $system->db();

$users = $db->query("SELECT * FROM users ORDER BY RAND()");

$time_max = strtotime('+7 days');
$time_min = strtotime('-7 days');

while($user = $users->fetch_object()) {
	$views = $db->query("SELECT COUNT(id) as count FROM events WHERE event='profile_view' AND profile_id='".$user->id."' AND time >= ".$time_min." AND time <= ".$time_max."");
	if($views->num_rows >= 1) {
		$views = $views->fetch_object();
		$views = $views->count;
		if($views >= 50) {
			$check = $db->query("SELECT id FROM owned_awards WHERE user_id='".$user->id."' AND award_id='1'");
			if($check->num_rows == 0) {
			$db->query("INSERT INTO owned_awards(user_id,award_id,time) VALUES (".$user->id.",'1','".time()."')");
			}
		} 
	}
	$likes = $db->query("SELECT COUNT(id) as count FROM events WHERE event='profile_like' AND profile_id='".$user->id."' AND time >= ".$time_min." AND time <= ".$time_max."");
	if($likes->num_rows >= 1) {
		$likes = $likes->fetch_object();
		$likes = $likes->count;
		if($likes >= 50) {
			$check = $db->query("SELECT id FROM owned_awards WHERE user_id='".$user->id."' AND award_id='2'");
			if($check->num_rows == 0) {
			$db->query("INSERT INTO owned_awards(user_id,award_id,time) VALUES (".$user->id.",'2','".time()."')");
			}
		} 
	}
	$sent_msg = $db->query("SELECT COUNT(id) as count FROM events WHERE event='sent_message' AND user_id='".$user->id."' AND time >= ".$time_min." AND time <= ".$time_max."");
	if($sent_msg->num_rows >= 1) {
		$sent_msg = $sent_msg->fetch_object();
		$sent_msg = $sent_msg->count;
		if($sent_msg >= 250) {
			$check = $db->query("SELECT id FROM owned_awards WHERE user_id='".$user->id."' AND award_id='3'");
			if($check->num_rows == 0) {
			$db->query("INSERT INTO owned_awards(user_id,award_id,time) VALUES (".$user->id.",'3','".time()."')");
			}
		} 
	}
}

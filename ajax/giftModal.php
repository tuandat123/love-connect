<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$db = $system->db();

$id = $_GET['id'];
$user_id = $_GET['user_id'];
$gift = $db->query("SELECT * FROM gifts WHERE id='".$id."'");
$gift = $gift->fetch_object();
$profile = new User($user_id);
echo 
'
<h3>
A gift from 
<a href="'.$system->getDomain().'/user/'.$profile->id.'">'
.$profile->full_name.'
</a>
<img src="'.$system->getDomain().'/uploads/'.$profile->profile_picture.'" class="img-circle">
</h3>
<img src="'.$system->getDomain().'/img/gifts/'.$gift->path.'">
<div class="note">
'.$gift->message.'
</div>
<a href="#" class="btn btn-primary btn-fill btn-lg btn-wd" data-toggle="modal" data-target="#send-gift" style="border: 5px double #eee;">
Send your gift 
</a>
';
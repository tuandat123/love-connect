<?php
session_set_cookie_params(172800);
session_start();
require('../../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ../index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_GET['skin'])) {
	$skin = $_GET['skin'];
	$db->query("UPDATE settings SET skin='".$skin."'");
}
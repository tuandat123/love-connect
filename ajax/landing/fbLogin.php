<?php
session_set_cookie_params(172800);
session_start();
require('../../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$id = $_POST['id'];
$full_name = $db->real_escape_string($_POST['name']);
$full_name = explode(' ',$full_name);
$first_name = $full_name[0];
$work = $db->real_escape_string($_POST['work']);
$education = $db->real_escape_string($_POST['education']);
$likes = $_POST['likes'];
$likes = json_decode($likes,true);
$likes = $likes['data'];
$friends = $_POST['friends'];

$exist = $db->query("SELECT * FROM users WHERE fb_id='".$id."'");
if($exist->num_rows >= 1) {
	$user = $exist->fetch_object();
	$_SESSION['auth'] = true;
	$_SESSION['email'] = $user->email;
	$_SESSION['user_id'] = $user->id;
	$_SESSION['full_name'] = $user->full_name;
	$_SESSION['is_admin'] = $user->is_admin;
	echo 'true';
} else {
	$query = $db->query("INSERT INTO users (fb_id,full_name,email,birthday,password,credits,registered,
		last_login,last_active,profile_picture,gender,here_to,age,work,education) VALUES ('".$id."','".$first_name."','','',
		'','0','".time()."','".time()."','".time()."','default_avatar.png',''
		,'1','','".$work."','".$education."')");
	$user_id = $db->insert_id;
	$_SESSION['auth'] = true;
	$_SESSION['email'] = $email;
	$_SESSION['user_id'] = $user_id;
	$_SESSION['full_name'] = $first_name;
	$_SESSION['is_admin'] = 0;
	if(!empty($_POST['latitude']) && !empty($_POST['longitude'])) {
		$geo = $system->reverseGeo();
		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];
		$city = $geo->city;
		$country = $geo->country_name;
		$ip = $_SERVER['REMOTE_ADDR'];
		$db->query("UPDATE users SET latitude='".$latitude."',longitude='".$longitude."',
			city='".$city."',country='".$country."',ip='".$ip."' WHERE id='".$user_id."'");
	} else {
		$geo = $system->reverseGeo();
		$latitude = $geo->latitude;
		$longitude = $geo->longitude;
		$city = $geo->city;
		$country = $geo->country_name;
		$ip = $_SERVER['REMOTE_ADDR'];
		$db->query("UPDATE users SET latitude='".$latitude."',longitude='".$longitude."',
			city='".$city."',country='".$country."',ip='".$ip."' WHERE id='".$user_id."'");
	}
	foreach ($likes as $like) {
		$name = $like['name'];
		$db->query("INSERT INTO interests(user_id,name) VALUES ('".$user_id."','".$name."')");
	}
	$db->query("INSERT INTO verifications(user_id,type,data,is_active,time) VALUES ('".$user_id."','facebook','".$friends."','1','".time()."')");
	if($query) {
		echo 'true';
	} else {
		echo 'false';
	}
}
<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$db = $system->db();

if(isset($_POST['work'])) {
	$work = $_POST['work'];
	$db->query("UPDATE users SET work='".$work."' WHERE id='".$my_user->id."'");
}

if(isset($_POST['education'])) {
	$education = $_POST['education'];
	$db->query("UPDATE users SET education='".$education."' WHERE id='".$my_user->id."'");
}

if(isset($_POST['friends'])) {
	$friends = $_POST['friends'];
	if(!$system->hasVerification('facebook')) {
	$db->query("INSERT INTO verifications(user_id,type,data,is_active,time) VALUES ('".$my_user->id."','facebook','".$friends."','1','".time()."')");
	}
}

if(isset($_POST['likes'])) {
	$likes = $_POST['likes'];
	$likes = json_decode($likes,true);
	$likes = $likes['data'];
	foreach ($likes as $like) {
		$name = $like['name'];
		$db->query("INSERT INTO interests(user_id,name) VALUES ('".$my_user->id."','".$name."')");
	}
}
<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$db = $system->db();

$sticker_packs = $db->query("SELECT * FROM sticker_packs ORDER BY id ASC");
$receiver_id = $_GET['receiver_id'];

while($sticker_pack = $sticker_packs->fetch_object()) {
	echo '
	<span class="emoji-top-img sticker-pack-'.$sticker_pack->id.' emj" onclick="loadStickers('.$sticker_pack->id.','.$receiver_id.','.$sticker_pack->is_premium.'); setActiveEmojiLink(this);">
	<img src="'.$system->getDomain().'/img/stickers/'.$sticker_pack->id.'/'.$sticker_pack->cover.'">
	</span>
	';
}
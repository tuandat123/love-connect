<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

$pack_id = $_GET['pack_id'];
$receiver_id = $_GET['receiver_id'];
$is_premium = $_GET['is_premium'];

$pack_info = $db->query("SELECT name FROM sticker_packs WHERE id='".$pack_id."'");
$pack_info = $pack_info->fetch_object();

$stickers = $db->query("SELECT * FROM stickers WHERE pack_id='".$pack_id."' ORDER BY id ASC");

if($is_premium == 0) {
	while($sticker = $stickers->fetch_object()) {
		echo '<img src="'.$system->getDomain().'/img/stickers/'.$sticker->pack_id.'/'.$sticker->path.'" onclick="sendSticker('.$sticker->id.','.$receiver_id.')" class="sticker">';
	}
} else {
	$is_owned = $db->query("SELECT id FROM owned_sticker_packs WHERE pack_id='".$pack_id."' AND user_id='".$my_user->id."' LIMIT 1");
	if($is_owned->num_rows >= 1) {
		while($sticker = $stickers->fetch_object()) {
			echo '<img src="'.$system->getDomain().'/img/stickers/'.$sticker->pack_id.'/'.$sticker->path.'" onclick="sendSticker('.$sticker->id.','.$receiver_id.')" class="sticker">';
		}	
	} else {
		echo '
		<div class="sticker-unlock-area">
		<a href="#" class="btn btn-primary btn-sm btn-fill" onclick="unlockStickers('.$pack_id.','.$receiver_id.','.$is_premium.')"> '.$system->translate('Unlock_Now').' </a>
		<p class="sticker-unlock-name"> <b>'.$pack_info->name.'</b> <span> '.$system->translate('sticker_pack').' </span> </p>
		<p class="sticker-disclaimer"> '.sprintf($system->translate('Service_Cost'),$system->settings->sticker_pack_price).' </p>
		</div>
		';
		echo '<div id="darkLayer-'.$pack_id.'" class="darkClass">';
		while($sticker = $stickers->fetch_object()) {
			echo '<img src="'.$system->getDomain().'/img/stickers/'.$sticker->pack_id.'/'.$sticker->path.'" class="sticker-disabled">';
		}
		echo '</div>';	
	}
}
<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

$id = $_GET['id'];

$messages = $db->query("SELECT * FROM messages WHERE (user1='".$my_user->id."' AND user2='".$id."') OR (user1='".$id."' AND user2='".$my_user->id."') ORDER BY id ASC");
$receiver = new User($id);
if($receiver->isOnline()) { 
	$online_status = '<i class="online-status online"></i>'; 
} else { 
	$online_status = '<i class="online-status offline"></i>';
}
$timestamps = array();
$receiver = 
'
<div class="chat-receiver-info">
<h4 class="chat-receiver-name">
'.$online_status.'
'.$receiver->full_name.', '.$receiver->age.'
</h4>
<div class="chat-receiver-actions pull-right">
<a class="btn btn-default btn-icon btn-sm btn-cog" onclick="deleteMessages('.$id.')" style="height: 2.3em; padding-top: 0.4em;">
<i class="icon icon-bin"></i>
</a>
</div>
</div>
';

$data = array('receiver' => $receiver, 'messages' => '');

while($message = $messages->fetch_object()) {
	$profile = new User($message->user1);
	if(!in_array(date('F j Y',$message->time), $timestamps)) {
		$data['messages'] .= '<div class="date text-muted">'.date('j F Y',$message->time).'</div>';
	}
	array_push($timestamps, date('F j Y',$message->time));
	if($my_user->id == $profile->id) {
		$db->query("UPDATE messages SET is_seen_1='1' WHERE id='".$message->id."'");
	} else {
		$db->query("UPDATE messages SET is_seen_2='1' WHERE id='".$message->id."'");
	}
	if($message->is_sticker == 1) {
		$sticker = $db->query("SELECT id,pack_id,path FROM stickers WHERE id='".$message->sticker_id."'");
		$sticker = $sticker->fetch_object();
		$data['messages'] .= '
		<div class="chat-message">
		<img src="'.$system->getDomain().'/uploads/'.$profile->profile_picture.'" class="chat-content-thumb img-circle no-padding pull-left">
		<p class="chat-content-username text-muted"> '.$profile->full_name.' </p>
		<p class="chat-content-text"> <img src="'.$system->getDomain().'/img/stickers/'.$sticker->pack_id.'/'.$sticker->path.'" class="sticker-chat"> </p>
		</div>
		';
	} else {
		$data['messages'] .= '
		<div class="chat-message">
		<img src="'.$system->getDomain().'/uploads/'.$profile->profile_picture.'" class="chat-content-thumb img-circle no-padding pull-left">
		<p class="chat-content-username text-muted"> '.$profile->full_name.' </p>
		<p class="chat-content-text"> '.htmlentities($message->message).' </p>
		</div>
		';
	}
}

echo json_encode($data);
<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

$image_data = $_FILES['file'];

if($image_data['name']) {
	$extension = strtolower(end(explode('.', $image_data['name'])));
	if($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg') {
		if(!$image_data['error']) {
			$new_file_name = md5(mt_rand()).'_'.$image_data['name'];
			if($image_data['size'] > (1024000)) {
				$valid_file = false;
				$error = 'Oops! One of the photos you uploaded is too large';
			} else {
				$valid_file = true;
			}
			if($valid_file) {
				move_uploaded_file($image_data['tmp_name'], '../uploads/'.$new_file_name);
				$db->query("INSERT INTO uploaded_photos (user_id,path,time) VALUES ('".$my_user->id."','".$new_file_name."','".time()."')");
			}
		} else {
			$error = 'Error occured:  '.$image_data['error'];
		}
	}	
}
if(!empty($error)) {
	echo $error;
}
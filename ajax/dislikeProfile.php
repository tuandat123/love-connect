<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$db = $system->db();

$id = $_GET['id'];

$check = $db->query("SELECT id FROM profile_dislikes WHERE viewer_id='".$my_user->id."' AND profile_id='".$id."' LIMIT 1");

$db->query("INSERT INTO profile_dislikes (profile_id,viewer_id,time) VALUES ('".$id."','".$my_user->id."','".time()."')");
$db->query("DELETE FROM profile_likes WHERE profile_id='".$id."' AND viewer_id='".$my_user->id."'");

if($my_user->canDislike($id)) {
	echo '
	<button class="btn btn-circle btn-primary btn-lg">
	<i class="icon icon-cross"></i>
	</button>
	';
} else { 
	echo '
	<button class="btn btn-circle btn-primary btn-lg" disabled>
	<i class="icon icon-cross"></i>
	</button>
	';
}
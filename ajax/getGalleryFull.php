<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$profile = new User($_GET['id']);
$db = $system->db();

$media = $profile->getPhotos();

if($media->num_rows >= 1) { 
while($photo = $media->fetch_object()) { 
echo '
<li data-thumb="'.$system->getDomain().'/uploads/'.$photo->path.'" data-src="'.$system->getDomain().'/uploads/'.$photo->path.'">
	<img src="'.$system->getDomain().'/uploads/'.$photo->path.'">
</li>
';
} 
} 
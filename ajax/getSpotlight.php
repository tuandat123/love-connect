<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$db = $system->db();

$spotlight_users = $db->query("SELECT id FROM users WHERE (is_featured='1' AND is_featured_exp > ".time().") ORDER BY RAND() LIMIT 15");

if(!$my_user->isPromoted('spotlight')) {
	echo '
	<a href="'.$system->getDomain().'/credits">
	<div class="box">  
	<img src="'.$system->getDomain().'/uploads/'.$my_user->profile_picture.'" class="img-circle no-padding">
	<div class="overbox">
	<div class="title overtext">
	<i class="icon icon-plus"></i>
	</div>
	</div>
	</div>
	</a>
	';
}
while($spotlight_user = $spotlight_users->fetch_object()) { 
	$profile = new User($spotlight_user->id);
	echo '
	<a href="'.$system->getDomain().'/user/'.$profile->id.'" class="spotlight-user">
	<img src="'.$system->getDomain().'/uploads/'.$profile->profile_picture.'" class="img-circle">
	'; 
	if($profile->isOnline()) { 
		echo '<i class="online-status online"></i>'; 
	}
	echo '
	</a>
	<div class="hidden"> '.$profile->full_name.' </div>
	';
}

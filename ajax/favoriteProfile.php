<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$db = $system->db();

$id = $_GET['id'];

$check = $db->query("SELECT id FROM profile_favorites WHERE viewer_id='".$my_user->id."' AND profile_id='".$id."' LIMIT 1");

if($check->num_rows >= 1) {
	$db->query("DELETE FROM profile_favorites WHERE profile_id='".$id."' AND viewer_id='".$my_user->id."'");
} else {
	$db->query("INSERT INTO profile_favorites (profile_id,viewer_id,time) VALUES ('".$id."','".$my_user->id."','".time()."')");
}

if($my_user->canFavorite($id)) {
	echo '
	<button class="btn btn-default btn-icon btn-lg btn-tooltip profile-control">
	<i class="icon icon-star-full"></i>
	</button>
	<div class="hidden"> Add to favorites </div>
	';
} else { 
	echo '
	<button class="btn btn-warning btn-fill btn-icon btn-lg btn-tooltip profile-control">
	<i class="icon icon-star-full"></i>
	</button>
	<div class="hidden"> Remove from favorites </div>
	';
}
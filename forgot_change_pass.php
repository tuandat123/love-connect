<?php
session_set_cookie_params(172800);
session_start();
require('core/classes.php');
$system = new Core;
$auth = new Auth;
$widget = new Widget;
$system->getLanguage();
$db = $system->db();

$languages = scandir('languages');
?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta charset="utf-8">
        <title><?= $system->setting('website_title') ?></title>
        <link rel="icon" type="image/png" href="<?= $system->getDomain() ?>/img/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?= $system->getDomain() ?>/img/favicon-16x16.png" sizes="16x16">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="<?= $system->setting('website_description') ?>">
        <meta name="keywords" content="<?= $system->setting('website_keywords') ?>">
        <meta property="fb:app_id" content="<?= $system->setting('fb_app_id') ?>">
        <meta property="og:type" content="website">
        <meta property="og:url" content="<?= $system->getDomain() ?>">
        <meta property="og:title" content="<?= $system->setting('website_title') ?>">
        <meta property="og:description" content="<?= $system->setting('website_description') ?>">
        <meta property="og:image" content="<?= $system->getDomain() ?>/img/logo-medium.png">  <link href="<?= $system->getDomain() ?>/assets/bootstrap3/css/bootstrap.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/css/plugins.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/css/theme.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/landing/landing.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/landing/login.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/fonts/icomoon/styles.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/fonts/themify/styles.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/fonts/linea/styles.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/css/style-animation-mail.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <script>
            var base = '<?= $system->getDomain() ?>';
        </script>
    </head>
    <body>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= $system->getDomain() ?>/index.php"><img src="<?= $system->getDomain() ?>/img/logo-color.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="language-select">
                                <select class="select2" onchange="selectLanguage(this); return false;">
                                    <?php foreach ($languages as $language) { ?>
                                        <?php if (file_exists('languages/' . $language . '/language.php')) { ?>
                                            <?php if (isset($_SESSION['language']) && $_SESSION['language'] == $language) { ?>
                                                <option value="<?= $language ?>" selected><?= ucfirst($language) ?></option>
                                                <? } else { ?>
                                                <option value="<?= $language ?>"><?= ucfirst($language) ?></option>
                                                <? } ?>
                                                <? } ?>
                                                <? } ?>
                                            </select>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a class="help"><?= $system->translate('Already_Member') ?></a>
                                    </li>
                                    <li>
                                        <a href="<?= $system->getDomain() ?>/index.php" class="btn btn-success btn-sm btn-fill btn-sig-ct-m"><?= $system->translate('index_7') ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <div class="container">
                        <div class="inner-container" style="box-shadow: 0 0px 0px rgba(0, 0, 0, 0) !important;">
                            <div id="login">
                                <h1> <?= $system->translate('Create_Password_New') ?> </h1>
                                <p> <?= $system->translate('Create_Password_New_Detail') ?> </b>
                                <form method="post" class="form-inline">
                                    <div class="form-group">
                                        <label> <?= $system->translate('Pass_Word_New') ?> </label>
                                        <input type="password" id="password-new" name="password" class="form-control pull-right" placeholder="<?= $system->translate('Pass_Word_New') ?> " autocomplete="off" >
                                        <div class="form-group" style="display: none;" id="form-group-message-pass-error">
                                            <small class="message-error" id="error-password" type="hidden" style="margin-left: 8em;"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--  <label> <?= $system->translate('Pass_Word_Confirm') ?> </label>-->
                                        <input type="password" id="password-new-confirm" name="password-confirm" class="form-control pull-right" placeholder="<?= $system->translate('Pass_Word_Confirm') ?>" autocomplete="off" >
                                        <div class="form-group" style="display: none;" id="form-group-message-pass-confirm-error">
                                            <small class="message-error" id="error-password-confirm" type="hidden" style="margin-left: 8em;"></small>
                                        </div>
                                    </div>
                                    <br>
                                    <button type="submit" name="forget_change_pass" class="btn btn-success btn-fill btn-wd btn-sm" id="forget-change-pass"><?= $system->translate('Submit_Change_Password') ?></button>
                                    <input type="hidden" id="url" name="url"  value=<?= $system->getDomain() ?> >
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <footer>
                    <?= $widget->footerPages() ?>
                </footer>
            </div>
            <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>
            <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
            <script src="<?= $system->getDomain() ?>/assets/bootstrap3/js/bootstrap.js" type="text/javascript"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
            <script src="<?= $system->getDomain() ?>/assets/landing/landing.js" type="text/javascript"></script>
            <script src="<?= $system->getDomain() ?>/assets/js/ajax-forget-change-pass.js" type="text/javascript" ></script>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
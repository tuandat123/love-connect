<?php
session_set_cookie_params(172800);
session_start();
require('core/classes.php');
$system = new Core;
$auth = new Auth;
$geo = new Geo;
$widget = new Widget;
$helper = new Helper;
$system->getLanguage();
$db = $system->db();

if(!$auth->isLogged()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
	$my_user->markActivity();
}

$do = array();
$page = $_GET['page'];

switch ($page) {
	case 'encounters':
	include('sources/encounters.php');
	break;
	case 'people':
	include('sources/people.php');
	break;
	case 'liked-you':
	include('sources/liked_you.php');
	break;
	case 'matches':
	include('sources/matches.php');
	break;
	case 'visited-you':
	include('sources/visited_you.php');
	break;
	case 'favorited-you':
	include('sources/favorited_you.php');
	break;
	case 'my-favorites':
	include('sources/my_favorites.php');
	break;
	case 'profile':
	include('sources/profile.php');
	break;
	case 'popularity':
	include('sources/popularity.php');
	break;
	case 'credits':
	include('sources/credits.php');
	break;
	case 'superpowers':
	include('sources/superpowers.php');
	break;
	case 'settings':
	include('sources/settings.php');
	break;
	case 'custom_page':
	include('sources/custom_page.php');
	break;
}
<?php
session_set_cookie_params(172800);
session_start();
require('core/classes.php');
$system = new Core;
$auth = new Auth;
$widget = new Widget;
$system->getLanguage();
$db = $system->db();

$languages = scandir('languages');

if(isset($_POST['login'])) {

  $email    = $db->real_escape_string($_POST['email']);
  $password = $db->real_escape_string($_POST['password']);

  if(!empty($email) && !empty($password)) {
    $user = $db->query("SELECT * FROM users WHERE email='".$email."'");
    if($user->num_rows >= 1) {
      $user = $user->fetch_object();
      if($user->password == $auth->hashPassword($password)) {
        $_SESSION['auth'] = true;
        $_SESSION['email'] = $email;
        $_SESSION['user_id'] = $user->id;
        $_SESSION['full_name'] = $user->full_name;
        $_SESSION['is_admin'] = $user->is_admin;
      } else {
        $error = array('message' => 'Invalid Password', 'type' => 'password');
      }
    } else {
      $error = array('message' => 'Email is not registered', 'type' => 'email');
    }
    if(isset($error)) {
      header('Location: '.$system->getDomain().'/login?error='.$error['message'].'&type='.$error['type']);
      exit;
    } else {
      header('Location: '.$system->getDomain().'/encounters');
      exit;
    }
  } else {
    header('Location: '.$system->getDomain().'/login');
    exit;
  }
}

if(isset($_GET['error']) && isset($_GET['type'])) {
  $error = array('message' => $_GET['error'], 'type' => $_GET['type']);
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta charset="utf-8">
  <title><?=$system->setting('website_title')?></title>
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?=$system->getDomain()?>/img/favicon-16x16.png" sizes="16x16">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="<?=$system->setting('website_description')?>">
  <meta name="keywords" content="<?=$system->setting('website_keywords')?>">
  <meta property="fb:app_id" content="<?=$system->setting('fb_app_id')?>">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?=$system->getDomain()?>">
  <meta property="og:title" content="<?=$system->setting('website_title')?>">
  <meta property="og:description" content="<?=$system->setting('website_description')?>">
  <meta property="og:image" content="<?=$system->getDomain()?>/img/logo-medium.png">  <link href="<?=$system->getDomain()?>/assets/bootstrap3/css/bootstrap.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/css/plugins.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/css/theme.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/landing/landing.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/landing/login.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/fonts/icomoon/styles.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/fonts/themify/styles.css" rel="stylesheet">
  <link href="<?=$system->getDomain()?>/assets/fonts/linea/styles.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <script>
  var base = '<?=$system->getDomain()?>';
  </script>
</head>
<body>
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?=$system->setting('fb_app_id')?>',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };
  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
  </script>
  <nav class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?=$system->getDomain()?>/index.php"><img src="<?=$system->getDomain()?>/img/logo-color.png"></a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-left">
          <li>
            <a href="#" class="language-select">
              <select class="select2" onchange="selectLanguage(this); return false;">
                <?php foreach($languages as $language) { ?>
                  <?php if(file_exists('languages/'.$language.'/language.php')) { ?>
                      <?php if(isset($_SESSION['language']) && $_SESSION['language'] == $language) { ?>
                      <option value="<?=$language?>" selected><?=ucfirst($language)?></option>
                      <? } else { ?>
                      <option value="<?=$language?>"><?=ucfirst($language)?></option>
                      <? } ?>
                  <? } ?>
                <? } ?>
              </select>
            </a>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a class="help"><?=$system->translate('Already_Member')?></a>
          </li>
          <li>
            <a href="<?=$system->getDomain()?>/index.php" class="btn btn-success btn-sm btn-fill btn-sig-ct-m"><?=$system->translate('index_7')?></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="inner-container" style="box-shadow: 0 0px 0px rgba(0, 0, 0, 0) !important;">
      <div id="login">
        <h1> <?=$system->translate('Sing_In_To')?> <?=$system->settings->website_name?> </h1>
        <p> <?=$system->translate('Enter_Details')?> </b>
          <form action="" method="post" class="form-inline">
            <div class="form-group">
              <label> <?=$system->translate('Email')?> </label>
              <?php if(isset($error) && $error['type'] == 'email') { ?>
              <input type="email" name="email" class="form-control pull-right input-error" placeholder="Your email" autocomplete="off" required> <br>
              <? } else { ?>
              <input type="email" name="email" class="form-control pull-right" placeholder="Your email" autocomplete="off" required>
              <? } ?>
            </div>
            <?php if(isset($error) && $error['type'] == 'email') { ?> <small class="text-danger"> <?=$error['message']?> </small> <? } ?>
            <div class="form-group">
              <label> <?=$system->translate('Password')?> </label>
              <?php if(isset($error) && $error['type'] == 'password') { ?>
              <input type="password" name="password" class="form-control pull-right input-error" placeholder="Your password" autocomplete="off" required>
              <? } else { ?>
              <input type="password" name="password" class="form-control pull-right" placeholder="Your password" autocomplete="off" required>
              <? } ?>
            </div>
            <?php if(isset($error) && $error['type'] == 'password') { ?> <small class="text-danger"> <?=$error['message']?> </small> <? } ?>
            <br>
            <button type="submit" name="login" class="btn btn-success btn-fill btn-wd btn-sm"><?=$system->translate('Sign_In')?></button>
            <!-- 
            Password reset will be implemented in a future version
            // Do not uncomment
            <a href="#" class="underline"> <?=$system->translate('Forgot_Pass')?> </a>
            -->
          </form>
        </div>
        <div id="social">
          <span class="or"><?=$system->translate('Or')?></span>
          <a href="#" class="btn btn-primary btn-fill btn-icon btn-sm" onclick="fbLogin(); return false;" style="border-color: #ffffff;"> 
            <i class="icon icon-facebook"></i> Facebook 
          </a>
        </div>
      </div>
    </div>
    <footer>
      <?=$widget->footerPages()?>
    </footer>
  </div>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>
  <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
  <script src="<?=$system->getDomain()?>/assets/bootstrap3/js/bootstrap.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script src="<?=$system->getDomain()?>/assets/landing/landing.js" type="text/javascript"></script>
</body>
</html>
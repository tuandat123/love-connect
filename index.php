<?php
session_set_cookie_params(172800);
session_start();
require('core/classes.php');
$system = new Core;
$widget = new Widget;
$system->getLanguage();
$db = $system->db();

$languages = scandir('languages');

?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta charset="utf-8">
        <title><?= $system->setting('website_title') ?></title>
        <link rel="icon" type="image/png" href="<?= $system->getDomain() ?>/img/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?= $system->getDomain() ?>/img/favicon-16x16.png" sizes="16x16">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="<?= $system->setting('website_description') ?>">
        <meta name="keywords" content="<?= $system->setting('website_keywords') ?>">
        <meta property="fb:app_id" content="<?= $system->setting('fb_app_id') ?>">
        <meta property="og:type" content="website">
        <meta property="og:url" content="<?= $system->getDomain() ?>">
        <meta property="og:title" content="<?= $system->setting('website_title') ?>">
        <meta property="og:description" content="<?= $system->setting('website_description') ?>">
        <meta property="og:image" content="<?= $system->getDomain() ?>/img/logo-medium.png">
        <link href="<?= $system->getDomain() ?>/assets/bootstrap3/css/bootstrap.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/css/plugins.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/css/theme.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/landing/landing.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/fonts/icomoon/styles.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/fonts/themify/styles.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/fonts/linea/styles.css" rel="stylesheet">
        <link href="<?= $system->getDomain() ?>/assets/css/style-backgroud.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
            var base = '<?= $system->getDomain() ?>';
        </script>
    </head>
    <body>
        <!-- @main page view -->
        <div class="view-main">
            <div class="header-wrapp">
                <div class="wave-wrapp">
                    <svg class="wave" xmlns="http://www.w3.org/2000/svg"  viewBox="0 24 150 28"   preserveAspectRatio="none">
                    <defs>
                    <path id="gentle-wave" d="m -160,44.4 c 30,0 58,-18 87.7,-18 30.3,0 58.3,18 87.3,18 30,0 58,-18 88,-18 30,0 58,18 88,18 l 0,34.5 -351,0 z" />
                    </defs>
                    <g class="parallax">
                    <use xlink:href="#gentle-wave" x="50" y="0" fill="rgba(255, 255, 255, 0.48)"/>
                    <use xlink:href="#gentle-wave" x="50" y="3" fill="rgba(255, 255, 255, 0.78)"/>
                    <use xlink:href="#gentle-wave" x="50" y="6" fill="rgba(255, 255, 255, 0.8)"/>  
                    </g>
                    </svg>
                </div>
            </div>
        </div>
        <script>
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '<?= $system->setting('fb_app_id') ?>',
                    xfbml: true,
                    version: 'v2.8'
                });
                FB.AppEvents.logPageView();
            };
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <nav class="navbar navbar-transparent" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only"><?= $system->translate('Toggle_Nav') ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= $system->getDomain() ?>/index.php"><img src="<?= $system->getDomain() ?>/img/logo.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="language-select">
                                <select class="select2" onchange="selectLanguage(this); return false;">
                                    <?php foreach ($languages as $language) { ?>
                                        <?php if (file_exists('languages/' . $language . '/language.php')) { ?>
                                            <?php if (isset($_SESSION['language']) && $_SESSION['language'] == $language) { ?>
                                                <option value="<?= $language ?>" selected><?= ucfirst($language) ?></option>
                                                <? } else { ?>
                                                <option value="<?= $language ?>"><?= ucfirst($language) ?></option>
                                                <? } ?>
                                                <? } ?>
                                                <? } ?>
                                            </select>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <!--
                                    Public profiles will be implemented in a future version
                                    // Do not uncomment
                                    <li>
                                      <a href="<?= $system->getDomain() ?>/login">
                                       <i class="linea linea-basic-cards-hearts xl"></i>
                                    <?= $system->translate('Encounters') ?>
                                     </a>
                                   </li>
                                   <li>
                                    <a href="<?= $system->getDomain() ?>/login">
                                      <i class="linea linea-basic-target"></i>
                                    <?= $system->translate('People_Nearby') ?>
                                    </a>
                                  </li>
                                    -->
                                    <li>
                                        <a href="<?= $system->getDomain() ?>/login" class="btn btn-default btn-sm btn-fill"><?= $system->translate('Log_In') ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <div class="container">
                        <div class="inner-container">
                            <div id="promo-half" class="pull-left">
                                <div id="counter">
                                    <?
                                    $count = $db->query("SELECT id FROM users");
                                    $count = $count->num_rows;
                                    $count = str_split($count);
                                    foreach ($count as $digit) {
                                    ?>
                                    <div class="digit"><?= $digit ?></div>
                                    <? } ?>
                                    <div class="info">
                                        <?= $system->translate('Already_Joined') ?>
                                        <div class="clearfix"></div>
                                        <a href="#" class="btn btn-facebook btn-lg" onclick="fbLogin(); return false;">
                                            <i class="icon icon-facebook"></i>
                                            <?= $system->translate('Sign_Via_FB') ?>
                                        </a>
                                        <div class="clearfix"></div>
                                        <small style="color: #4d4d4d;"> <?= $system->translate('Wont_Post') ?> </small>
                                    </div>
                                </div>
                                <div id="or"><?= $system->translate('Or') ?></div>
                            </div>
                            <div id="main-half" class="pull-right">
                                <div id="step-1">
                                    <?php if ($system->settings->can_register == 1) { ?>
                                        <span><?= $system->translate('Answer_Questions') ?></span>
                                        <span><?= $system->translate('You_Are') ?></span>
                                        <a href="#" class="btn btn-default btn-wd btn-fill btn-fill-ct" onclick="nextStep(2, this);" data-value="Male"> <?= $system->translate('Male') ?> </a>
                                        <div class="clearfix"></div>
                                        <a style="margin-bottom: 1em;" href="#" class="btn btn-default btn-wd btn-fill btn-fill-ct" onclick="nextStep(2, this);" data-value="Female"> <?= $system->translate('Female') ?> </a>
                                        <? } else { ?>
                                        <span><?= $system->translate('Reg_Not_Available') ?></span>
                                        <? } ?>
                                        <div class="clearfix"></div>
                                         <a href="<?= $system->getDomain() ?>/forgot" class="link-forgot-pass"> <?= $system->translate('forgot_your_password') ?> </a>
                                    </div>
                                    <div id="step-2" style="display:none;">
                                        <span><?= $system->translate('Here_To') ?></span>
                                        <a href="#" class="btn btn-default btn-wd btn-fill" onclick="nextStep(3, this);" data-value="2"> <?= $system->translate('Here_Chat') ?> </a>
                                        <div class="clearfix"></div>
                                        <a href="#" class="btn btn-default btn-wd btn-fill" onclick="nextStep(3, this);" data-value="1"> <?= $system->translate('Here_Friends') ?> </a>
                                        <div class="clearfix"></div>
                                        <a href="#" class="btn btn-default btn-wd btn-fill" onclick="nextStep(3, this);" data-value="3"> <?= $system->translate('Here_Date') ?> </a>
                                    </div>
                                    <div id="step-3" style="display:none;">
                                        <span class="title"><?= $system->translate('Almost_Done') ?></span>
                                        <form method="post" id="form-create-user">
                                            <div class="form-group">
                                                <input type="text" name="first_name" placeholder="Your first name" class="form-control" maxlength="30">
                                            </div>
                                            <div class="form-group">
                                                <small class="message-error" id="error-name" type="hidden" ></small>
                                            </div> 
                                            <div class="form-group">
                                                <input type="email" name="email" placeholder="Your email" class="form-control" autocomplete="off"  maxlength="100">
                                            </div>
                                            <div class="form-group">
                                                <small class="message-error" id="error-email" type="hidden" ></small>
                                            </div>
                                            <div class="form-group birthday">
                                                <select name="birth_day" id="birth-day" class="form-control select2" >
                                                    <option value=""><?= $system->translate('day') ?></option>
                                                    <?php
                                                    for ($i = 1; $i <= 31; $i++) {
                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                                <select name="birth_month" id="birth-month" class="form-control select2" >
                                                    <option value=""><?= $system->translate('month') ?></option>
                                                    <option value="1"><?= $system->translate('January') ?></option>
                                                    <option value="2"><?= $system->translate('February') ?></option>
                                                    <option value="3"><?= $system->translate('March') ?></option>
                                                    <option value="4"><?= $system->translate('April') ?></option>
                                                    <option value="5"><?= $system->translate('May') ?></option>
                                                    <option value="6"><?= $system->translate('June') ?></option>
                                                    <option value="7"><?= $system->translate('July') ?></option>
                                                    <option value="8"><?= $system->translate('August') ?></option>
                                                    <option value="9"><?= $system->translate('September') ?></option>
                                                    <option value="10"><?= $system->translate('October') ?></option>
                                                    <option value="11"><?= $system->translate('November') ?></option>
                                                    <option value="12"><?= $system->translate('December') ?></option>
                                                </select>
                                                <select name="birth_year" id="birth-year" class="form-control select2" >
                                                    <option value=""><?= $system->translate('Year') ?></option>
                                                    <?php
                                                    for ($i = 1920; $i <= 2017; $i++) {
                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <small class="message-error" id="error-birth-day" type="hidden" ></small>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" placeholder="Enter a password" class="form-control" maxlength="123">
                                            </div>
                                            <div class="form-group">
                                                <small class="message-error" id="error-password" type="hidden" ></small>
                                            </div>
                                            <button style="background-color: #cc208e !important;border-color: #ffffff;" type="submit" name="create_account" class="btn btn-primary btn-fill btn-wd btn-lg" id="btn-create-user"><?= $system->translate('index_2') ?></button>
                                            <input type="hidden" id="gender" name="gender" value=<?= $gender ?> >
                                            <input type="hidden" id="here_to" name="here_to"  value=<?= $here_to ?> > 
                                            <input type="hidden" id="url" name="url"  value=<?= $system->getDomain() ?> >
                                            <input type="hidden" id="country" name="country"  value="" >
                                            <input type="hidden" id="city" name="city"  value="" >
                                        </form>
                                    </div>
                                    <div id="steps">
                                        <div class="active"></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="map"></div>
                        
                        <div class="text-center">
                            <?= $system->setting('ad_1') ?>
                        </div>
                        <footer>
                            <?= $widget->footerPages() ?>
                        </footer>
                        <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>
                        <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
                        <script src="<?= $system->getDomain() ?>/assets/bootstrap3/js/bootstrap.js" type="text/javascript"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                        <script src="<?= $system->getDomain() ?>/assets/landing/landing.js" type="text/javascript"></script>
                        <script src="<?= $system->getDomain() ?>/assets/js/mobile.js" type="text/javascript"></script>
                        <script src="<?= $system->getDomain() ?>/assets/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
                        <script src="<?= $system->getDomain() ?>/assets/js/ajax-create-user.js" type="text/javascript" ></script>
                        <script type="text/javascript">
                            var map;
                            function initMap() {
                                var mapCenter = new google.maps.LatLng(47.6145, -122.3418); //Google map Coordinates
                                map = new google.maps.Map($("#map")[0], {
                                    center: mapCenter,
                                    zoom: 8
                                });
                            }

                            if ("geolocation" in navigator) {
                                navigator.geolocation.getCurrentPosition(function (position) {
                                    infoWindow = new google.maps.InfoWindow({map: map});
                                    var pos = {lat: position.coords.latitude, lng: position.coords.longitude};
                                    infoWindow.setPosition(pos);
                                    map.panTo(pos);
                                    var language = "en";
                                    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&sensor=true&language=" + language;
                                    $.ajax({
                                        type: 'GET',
                                        url: url,
                                        success: function (data) {
                                            var country = JSON.stringify(data['results'][0]['address_components'][6]['long_name']);
                                            var city = JSON.stringify(data['results'][4]['address_components'][0]['long_name']);
                                            
                                            $("#city").val(city);
                                            $("#country").val(country);
                                        }
                                    });
                                });
                            } else {
                                console.log("Browser doesn't support geolocation!");
                            }
                        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNemavsaKIgaY-sy1sXAVOSTAgfWnvn8A&callback=initMap" async defer></script>
    </body>

</html>
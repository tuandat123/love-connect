<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_GET['id']) && isset($_GET['action'])) {
	$id = $_GET['id'];
	$action = $_GET['action'];
	if($action == 'true') {
		$db->query("UPDATE verifications SET is_active='1' WHERE id='".$id."'");
	} else {
		$db->query("DELETE FROM verifications WHERE id='".$id."'");
	}
	header('Location: photo_verification.php');
	exit;
}
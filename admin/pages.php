<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['pages'] = 'active';
$page['name'] = 'Manage Pages';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

$pages = $db->query("SELECT * FROM pages ORDER BY id DESC");

require('../layout/admin/header.phtml');
require('../layout/admin/pages.phtml');
require('../layout/admin/footer.phtml');
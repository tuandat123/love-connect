<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_GET['id'])) {
	$id = $_GET['id'];
	$path = $_GET['path'];
	$db->query("DELETE FROM uploaded_photos WHERE id='".$id."'");
	unlink('../uploads/'.$path);
	header('Location: photos.php?success');
	exit;
}

<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_GET['id']) && isset($_GET['action'])) {
	$id = $_GET['id'];
	$action = $_GET['action'];
	if($action == 'delete_photo') {
		$report = $db->query("SELECT * FROM photo_reports WHERE id='".$id."'");
		if($report->num_rows >= 1) {
			$report = $report->fetch_object();
			$photo_path = $report->photo_path;
			$db->query("DELETE FROM uploaded_photos WHERE path='".$photo_path."'");
			unlink('../uploads/'.$photo_path);
		}
	} else {
		$db->query("DELETE FROM photo_reports WHERE id='".$id."'");
	}
	header('Location: photo_reports.php');
	exit;
}
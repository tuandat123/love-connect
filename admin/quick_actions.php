<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_GET['action'])) {
	$action = $_GET['action'];
	if($action == 1) {
		$db->query("DELETE FROM users WHERE email='fake@fake.com'");
	} elseif($action == 2) {
		$db->query("UPDATE users SET last_active='".time()."'");
	} elseif($action == 3) {
		$db->query("UPDATE users SET last_active='".time()."' WHERE email='fake@fake.com'");
	} elseif($action == 4) {
		$db->query("DELETE FROM users WHERE profile_picture='default_avatar.png'");
	}	
	header('Location: users.php');
	exit;
}

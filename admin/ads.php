<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['ads'] = 'active';
$page['name'] = 'Manage Ads';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_POST['save'])) {
	$ad_1 = $db->real_escape_string($_POST['ad_1']);
	$ad_2 = $db->real_escape_string($_POST['ad_2']);
	$db->query("UPDATE settings SET ad_1='".$ad_1."',ad_2='".$ad_2."'");
	header('Location: ads.php');
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/ads.phtml');
require('../layout/admin/footer.phtml');
<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['photos'] = 'active';
$page['name'] = 'Manage Photos';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

$photos = $db->query("SELECT * FROM verifications WHERE type='photo' AND is_active='0' ORDER BY id DESC");

require('../layout/admin/header.phtml');
require('../layout/admin/photo_verification.phtml');
require('../inc/admin/bottom.phtml');
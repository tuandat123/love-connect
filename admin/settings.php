<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core($db,$domain);
$auth = new Auth($db,$domain);
$db = $system->db();

$menu['settings'] = 'active';
$page['name'] = 'General Settings';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_POST['save_1'])) {
	$website_name = $db->real_escape_string($_POST['website_name']);
	$website_title = $db->real_escape_string($_POST['website_title']);
	$website_description = $db->real_escape_string($_POST['website_description']);
	$website_keywords = $db->real_escape_string($_POST['website_keywords']);
	$can_register = $_POST['can_register'];
	$minimum_age = $_POST['minimum_age'];
	$db->query("UPDATE settings SET website_name='".$website_name."',website_title='".$website_title."',website_description='".$website_description."',
		website_keywords='".$website_keywords."',can_register='".$can_register."',minimum_age='".$minimum_age."'");
	header('Location: settings.php');
	exit;
}

if(isset($_POST['save_2'])) {
	$fb_app_id = $db->real_escape_string($_POST['fb_app_id']);
	$google_maps_api_key = $db->real_escape_string($_POST['google_maps_api_key']);
	$db->query("UPDATE settings SET fb_app_id='".$fb_app_id."',google_maps_api_key='".$google_maps_api_key."'");
	header('Location: settings.php');
	exit;
}

if(isset($_POST['save_3'])) {
	$email_sender = $_POST['email_sender'];
	$smtp_host = $db->real_escape_string($_POST['smtp_host']);
	$smtp_username = $db->real_escape_string($_POST['smtp_username']);
	$smtp_password = $db->real_escape_string($_POST['smtp_password']);
	$smtp_encryption = $_POST['smtp_encryption'];
	$smtp_port = $db->real_escape_string($_POST['smtp_port']);
	$db->query("UPDATE settings SET email_sender='".$email_sender."',smtp_host='".$smtp_host."',smtp_username='".$smtp_username."',
		smtp_password='".$smtp_password."',smtp_encryption='".$smtp_encryption."',smtp_port='".$smtp_port."'");
	header('Location: settings.php');
	exit;
}

if(isset($_POST['save_4'])) {
	$unit_height = $db->real_escape_string($_POST['unit_height']);
	$unit_weight = $db->real_escape_string($_POST['unit_weight']);
	$db->query("UPDATE settings SET unit_height='".$unit_height."',unit_weight='".$unit_weight."'");
	header('Location: settings.php');
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/settings.phtml');
require('../layout/admin/footer.phtml');
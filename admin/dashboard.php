<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['dashboard'] = 'active';
$page['name'] = 'Dashboard';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

$user_count = $db->query("SELECT id FROM users");
$user_count = $user_count->num_rows;

$photo_count = $db->query("SELECT id FROM uploaded_photos");
$photo_count = $photo_count->num_rows;

$online_users = "SELECT * FROM users WHERE (".time()." - last_active) <= 300";
$online_users_count = $db->query($online_users)->num_rows;
$online_users = $db->query($online_users.' LIMIT 12');

$newest_users = $db->query("SELECT * FROM users ORDER BY id DESC LIMIT 12");

require('../layout/admin/header.phtml');
require('../layout/admin/dashboard.phtml');
require('../layout/admin/footer.phtml');
<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core($db,$domain);
$auth = new Auth($db,$domain);
$db = $system->db();

$menu['payments'] = 'active';
$page['name'] = 'Super Powers';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_POST['save'])) {
	$superpowers_1_week = $db->real_escape_string($_POST['superpowers_1_week']);
	$superpowers_1_month = $_POST['superpowers_1_month'];
	$superpowers_3_months = $_POST['superpowers_3_months'];
	$db->query("
		UPDATE settings SET 
		superpowers_1_week='".$superpowers_1_week."',
		superpowers_1_month='".$superpowers_1_month."',
		superpowers_3_months='".$superpowers_3_months."'"
		);
	header('Location: superpowers.php');
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/superpowers.phtml');
require('../layout/admin/footer.phtml');
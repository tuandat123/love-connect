<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;

if($auth->isLogged() && $auth->isAdmin()) {
	header('Location: dashboard.php');
	exit;
} else {
	header('Location: ../index.php');
	exit;
}
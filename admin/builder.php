<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['builder'] = 'active';
$page['name'] = 'Builder';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

require('../layout/admin/header.phtml');
require('../layout/admin/builder.phtml');
require('../layout/admin/footer.phtml');
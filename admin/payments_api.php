<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core($db,$domain);
$auth = new Auth($db,$domain);
$db = $system->db();

$menu['payments'] = 'active';
$page['name'] = 'Payment API(s)';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_POST['save'])) {
	$paypal_email = $db->real_escape_string($_POST['paypal_email']);
	$stripe_publishable_key = $_POST['stripe_publishable_key'];
	$stripe_secret_key = $_POST['stripe_secret_key'];
	$fortumo_credits_id = $_POST['fortumo_credits_id'];
	$fortumo_credits_secret = $_POST['fortumo_credits_secret'];
	$fortumo_superpowers_id = $_POST['fortumo_superpowers_id'];
	$fortumo_superpowers_secret = $_POST['fortumo_superpowers_secret'];
	$db->query("
		UPDATE settings SET 
		paypal_email='".$paypal_email."',
		stripe_publishable_key='".$stripe_publishable_key."',
		stripe_secret_key='".$stripe_secret_key."',
		fortumo_credits_id='".$fortumo_credits_id."',
		fortumo_credits_secret='".$fortumo_credits_secret."',
		fortumo_superpowers_id='".$fortumo_superpowers_id."',
		fortumo_superpowers_secret='".$fortumo_superpowers_secret."'"
		);
	header('Location: payments_api.php');
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/payment_api.phtml');
require('../layout/admin/footer.phtml');
<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['pages'] = 'active';
$page['name'] = 'Edit Page';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

$id = $_GET['id'];
$custom_page = $db->query("SELECT * FROM pages WHERE id='".$id."'");
$custom_page = $custom_page->fetch_object();

if(isset($_POST['save'])) {
	$page_title = $_POST['page_title'];
	$content = $db->real_escape_string($_POST['content']);
	$db->query("UPDATE pages SET page_title='".$page_title."',content='".$content."' WHERE id='".$id."'");
	header("Location: pages.php");
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/edit_page.phtml');
require('../layout/admin/footer.phtml');
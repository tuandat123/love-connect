<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core($db,$domain);
$auth = new Auth($db,$domain);
$db = $system->db();

$menu['photos'] = 'active';
$page['name'] = 'Manage Photos';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

$photos = $db->query("SELECT * FROM uploaded_photos ORDER BY id DESC");

require('../layout/admin/header.phtml');
require('../layout/admin/uploaded_photos.phtml');
require('../layout/admin/footer.phtml');
<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core($db,$domain);
$auth = new Auth($db,$domain);
$db = $system->db();

$menu['users'] = 'active';
$page['name'] = 'Manage Users';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(!isset($_POST['search'])) {
	$query = '';
	$users = "SELECT id FROM users ORDER BY id DESC";
} else {
	$query = ucfirst(strtolower($_POST['query']));
	$users = "SELECT id FROM users WHERE full_name LIKE '%".$query."%' ORDER BY id DESC";	
}

$users = $db->query($users);

require('../layout/admin/header.phtml');
require('../layout/admin/users.phtml');
require('../layout/admin/footer.phtml');
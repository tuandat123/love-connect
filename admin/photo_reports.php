<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['reports'] = 'active';
$page['name'] = 'Photo Reports';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

$reports = $db->query("SELECT * FROM photo_reports ORDER BY id DESC");

require('../layout/admin/header.phtml');
require('../layout/admin/photo_reports.phtml');
require('../layout/admin/footer.phtml');
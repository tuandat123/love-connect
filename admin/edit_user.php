<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['users'] = 'active';
$page['name'] = 'Edit User';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

$id = $_GET['id'];

$profile = new User($id);

if(isset($_POST['save'])) {
	$email = $_POST['email'];
	$full_name = $_POST['full_name'];
	$here_to = $_POST['here_to'];
	$bio = $_POST['bio'];
	$password = $_POST['password'];
	$credits = $_POST['credits'];
	$city = $_POST['city'];
	$country = $_POST['country'];
	$gender = $_POST['gender'];
	$age = $_POST['age'];
	$languages = $_POST['languages'];
	$work = $_POST['work'];
	$education = $_POST['education'];

	switch ($_POST['is_admin']) {
		case 'true':
		$is_admin = 1;
		break;
		case 'false':
		$is_admin = 0;
		break;
		default:
		$is_admin = 0;
		break;
	}

	if(empty($password)) {
		$password = $my_user->password;
	} else {
		$password = $auth->hashPassword($password);	
	}	

	$db->query("
		UPDATE users SET 
		email='".$email."',
		full_name='".$full_name."',
		here_to='".$here_to."',
		bio='".$bio."',
		password='".$password."',
		gender='".$gender."',
		credits='".$credits."',
		country='".$country."',
		city='".$city."',
		age='".$age."',
		languages='".$languages."',
		work='".$work."',
		education='".$education."',
		is_admin='".$is_admin."'
		WHERE id='".$id."'"
		);

	header('Location: users.php?success');
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/edit_user.phtml');
require('../layout/admin/footer.phtml');
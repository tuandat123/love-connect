<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

$menu['pages'] = 'active';
$page['name'] = 'Add Page';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ../index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_POST['add'])) {
	$page_title = $_POST['page_title'];
	$content = $db->real_escape_string(nl2br($_POST['content']));
	$db->query("INSERT INTO pages(page_title,content) VALUES ('".$page_title."','".$content."')");
	header('Location: pages.php');
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/add_page.phtml');
require('../layout/admin/footer.phtml');
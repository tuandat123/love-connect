<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core;
$auth = new Auth;
$db = $system->db();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_GET['id'])) {
	$id = $_GET['id'];
	$db->query("DELETE FROM users WHERE id='".$id."'");
	$db->query("DELETE FROM profile_likes WHERE profile_id='".$id."'");
	$db->query("DELETE FROM profile_views WHERE profile_id='".$id."'");
	$db->query("DELETE FROM profile_favorites WHERE profile_id='".$id."'");
	$db->query("DELETE FROM messages WHERE user1='".$id."' OR user2='".$id."'");
	header('Location: users.php');
	exit;
}

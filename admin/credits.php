<?php
session_set_cookie_params(172800);
session_start();
require('../core/config.php');
require('../core/classes.php');
$system = new Core($db,$domain);
$auth = new Auth($db,$domain);
$db = $system->db();

$menu['payments'] = 'active';
$page['name'] = 'Credits';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: '.$system->getDomain().'/index.php');
	exit;
} else {
	$my_user = new User($_SESSION['user_id']);
}

if(isset($_POST['save'])) {
	$credits_price_500 = $db->real_escape_string($_POST['credits_price_500']);
	$credits_price_1000 = $_POST['credits_price_1000'];
	$credits_price_2000 = $_POST['credits_price_2000'];
	$db->query("
		UPDATE settings SET 
		credits_price_500='".$credits_price_500."',
		credits_price_1000='".$credits_price_1000."',
		credits_price_2000='".$credits_price_2000."'"
		);
	header('Location: credits.php');
	exit;
}

require('../layout/admin/header.phtml');
require('../layout/admin/credits.phtml');
require('../layout/admin/footer.phtml');
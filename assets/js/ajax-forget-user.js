var forgotAccount = new function () {

    this.models = {
        btnCreateAccount: "#forget-pass",
        error_email : "#error-email",
        form_group_message_error : "#form-group-message-error",
        inputUrl : "#url",
        animationMail : ".animation-mail"
    };

    this.init = function () {
       
        $(forgotAccount.models.btnCreateAccount).click(function () {
            var isCheck;
            
            isCheck = forgotAccount.events.ajaxForgetUser();
            if (!isCheck) {
                return false;
            }
        });
    };

    this.events = {
        ajaxForgetUser: function () {
            
            // get url
            var url, email, label_error_email, form_group_message_error, isEmail;

            var email_regExp = new RegExp(/^([A-Z0-9.%+-]+@@[A-Z0-9.-]+.[A-Z]{2,6})*([,;][\s]*([A-Z0-9.%+-]+@@[A-Z0-9.-]+.[A-Z]{2,6}))*$/i);

            url = $(forgotAccount.models.inputUrl).val();

            // Input value
            email = $("input[name='email']").val();
            
            form_group_message_error = $(forgotAccount.models.form_group_message_error);
          
            // Span error message
            label_error_email = $(forgotAccount.models.error_email);

            // Message error
            var message_email, message_format;
 
            message_email = "* Please enter your email";
            message_format = "* Please enter valid email";
            
            isEmail = true;
            // Check null input
            // Email
            if (!$.trim(email).length) {
                label_error_email.show();
                form_group_message_error.show();
                label_error_email.html(message_email);
                $("input[name='email']").addClass('border-input-error');
                isEmail = false;
            } else {
                if (!forgotAccount.events.isValidEmailAddress(email)) {
                    label_error_email.show();
                    form_group_message_error.show();
                    label_error_email.html(message_format);
                    $("input[name='email']").addClass('border-input-error');
                    isEmail = false;
                } else if ($.trim(email).length > 100) {
                    message_format = "* Please enter your email 1 - 100 character";
                    label_error_email.show();
                    form_group_message_error.show();
                    label_error_email.html(message_format);
                    $("input[name='email']").addClass('border-input-error');
                    isEmail = false;
                } else {
                    label_error_email.hide();
                    form_group_message_error.hide();
                    $("input[name='email']").removeClass('border-input-error');
                }
            }
            
            if (!isEmail) { 
                return false;
            }
            
            $(forgotAccount.models.animationMail).show();
            
            $.ajax({
                type: "POST",
                url: url + '/forget_account.php',
                dataType: 'json',
                data: {
                    forgot_pass: "forgot_pass",
                    email: email
                }
            }).done(function (data) {
                
                var isEmai;
                isEmai = true;
                
                if (data['error_message'] != null && data['type_message'] == "email") {
                    form_group_message_error.show();
                    label_error_email.show();
                    label_error_email.html(data['error_message']);
                    $("input[name='email']").addClass('border-input-error');
                    isEmai = false;
                } 
                
                $(forgotAccount.models.animationMail).hide();
                
                swal( "" ,  data['type_message'] ,  "success", {
                    button: false
                });
                
                
            }).fail(function (error) {
                console.log(error);
            });
        },

        isValidEmailAddress: function (emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        }
    };
};

$(document).ready(function () {
    forgotAccount.init();
});
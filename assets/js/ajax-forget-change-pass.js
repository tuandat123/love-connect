var changePassword = new function () {

    this.models = {
        btnChangePass: "#forget-change-pass",
        //============
        formMessagePass : "#form-group-message-pass-error",
        formMessagePassConfirm : "#form-group-message-pass-confirm-error",
        //============
        span_error_password : "#error-password",
        span_error_password_confirm : "#error-password-confirm",
        //============
        input_pass : "#password-new",
        input_pass_confirm : "#password-new-confirm",
        //============
        inputUrl : "#url"
    };

    this.init = function () {
       
        $(changePassword.models.btnChangePass).click(function () {
            
            var isCheck;
            
            isCheck = changePassword.events.ajaxChangePass();
            
            if (!isCheck) {
                return false;
            }
        });
    };

    this.events = {
        ajaxChangePass : function () {
            
            var messagePassword,
                messagePasswordConfirm,
                spanErrorPassword,
                spanErrorPasswordConfirm,
                formMessagePass,
                formMessagePassConfirm,
                inputPass,
                inputPassConfirm,
                isPass,
                url;
        
            messagePassword = "* Please enter new password";
            messagePasswordConfirm = "* Please enter password confirm";
            
            spanErrorPassword = $(changePassword.models.span_error_password);
            spanErrorPasswordConfirm = $(changePassword.models.span_error_password_confirm);
            
            formMessagePass = $(changePassword.models.formMessagePass);
            formMessagePassConfirm = $(changePassword.models.formMessagePassConfirm);
            
            inputPass = $(changePassword.models.input_pass);
            inputPassConfirm = $(changePassword.models.input_pass_confirm);
            
            // Password
            if (!$.trim(inputPass.val()).length) {
                spanErrorPassword.show();
                formMessagePass.show();
                spanErrorPassword.html(messagePassword);
                inputPass.addClass('border-input-error');
                isPass = false;
            } else {
                if ($.trim(inputPass.val()).length > 123) {
                    messagePassword = "* Please enter your password 1 - 123 character";
                    spanErrorPassword.show();
                    formMessagePass.show();
                    spanErrorPassword.html(messagePassword);
                    inputPass.addClass('border-input-error');
                    isPass = false;
                } else {
                    spanErrorPassword.hide();
                    formMessagePass.hide();
                    inputPass.removeClass('border-input-error');
                }
            }
            
            // Password Confirm
            if (!$.trim(inputPassConfirm.val()).length) {
                spanErrorPasswordConfirm.show();
                formMessagePassConfirm.show();
                spanErrorPasswordConfirm.html(messagePasswordConfirm);
                inputPassConfirm.addClass('border-input-error');
                isPass = false;
            } else {
                if ($.trim(inputPassConfirm.val()).length > 123) {
                    messagePasswordConfirm = "* Please enter your password confirm 1 - 123 character";
                    spanErrorPasswordConfirm.show();
                    formMessagePassConfirm.show();
                    spanErrorPasswordConfirm.html(messagePasswordConfirm);
                    inputPassConfirm.addClass('border-input-error');
                    isPass = false;
                } else {
                    if ($.trim(inputPass.val()) != $.trim(inputPassConfirm.val())) {
                        messagePasswordConfirm = "* Please enter your password confirm like password";
                        spanErrorPasswordConfirm.show();
                        formMessagePassConfirm.show();
                        spanErrorPasswordConfirm.html(messagePasswordConfirm);
                        inputPassConfirm.addClass('border-input-error');
                        isPass = false;
                    } else {
                        spanErrorPasswordConfirm.hide();
                        formMessagePassConfirm.hide();
                        inputPassConfirm.removeClass('border-input-error');
                    }
                }
            }
            
            if  (isPass) {
                
                url = $(changePassword.models.inputUrl).val();
                
                $.ajax({
                type: "POST",
                url: url + '/forget_change_pass.php',
                dataType: 'json',
                data: {
                    change_pass: "change_pass",
                    pass: inputPass.val(),
                    passConfirm: inputPassConfirm.val()
                }
                }).done(function (data) {

                   
                }).fail(function (error) {
                    console.log(error);
                });
            }
        }
    };
};

$(document).ready(function () {
    changePassword.init();
});
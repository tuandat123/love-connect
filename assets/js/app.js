Dropzone.autoDiscover = false;
$(document).ready(function() {

  $('.profile-gallery').bxSlider({
    minSlides: 2,
    maxSlides: 5,
    slideWidth: 150,
    slideMargin: 0,
    pager: false,
    preloadImages: 'all',
    controls: true,
    infiniteLoop: false,
    mode: 'horizontal',
    prevText: '<i class="ti-angle-left encounter-control"></i>',
    nextText: '<i class="ti-angle-right encounter-control"></i>',
    hideControlOnEnd: true
  });

  var encounters = $('#encounterGallery ul').lightSlider({
    gallery: false,
    prevHtml: '<i class="ti-angle-left encounter-control" onclick="prevSlide(); return false;"></i>',
    nextHtml: '<i class="ti-angle-right encounter-control" onclick="nextSlide(); return false;"></i>',
    item: 1,
    loop: false,
    thumbItem:9,
    slideMargin:0,
    enableDrag: false,
    currentPagerPosition:'left',
    controls: true,
    vertical: false,
    verticalHeight:640,
    vThumbWidth: 60,
    pager: false,
    mode: 'fade',
    onSliderLoad: function(el) {
      var slide = $('.lslide.active').attr('data-src');
      var memory = $('#reported-photo');
      if(memory.length) {
        memory.val($('.lslide.active').attr('data-path'));
      }
    },
  });

  $('.gift-selection').mCustomScrollbar({
    theme: 'light-3',
    live: 'on',
  });

  if($('.location-autocomplete').length) {
    new TeleportAutocomplete({ el: '.location-autocomplete', maxItems: 4 });
  }

  $('.btn-tooltip').each(function() {
    $(this).qtip({
      content: {
        text: $(this).next('div').html()
      },
      position: {
        at: 'bottom center',
      },
      style: {
        corner: false,
        classes: ''
      },
    });
  });

  var percentage = $('.score-percentage').val();
  if(percentage == 'NAN') {
    percentage = 0;
  }
  $('.score-chart').circleProgress({
    value: percentage,
    size: 70,
    fill: {
      gradient: ['#2B65F8', '#F02139']
    },
  });

  $('.chosen').each(function(){
    $(this).select2({minimumResultsForSearch: -1});
  });

  $('#search').keyup(function() {
    var search = $('#search');
    var query = search.val().toLowerCase();
    if(query.length > 0) {
      $('.chat-sidebar-username').each(function() { 
        var html = $(this).html().toLowerCase();
        if(html.indexOf(query) >= 0) {
          $(this).parent().parent().show();
        } else {
          $(this).parent().parent().hide();
        }
      });
    } else {
      $('.chat-sidebar-username').each(function() { 
        $(this).parent().parent().show();
      });
    }
  });

  $('.pixelate').each(function() {
    $(this).pixelate();
  });

  getSpotlight();
  var size = $('.lslide').size();
  var index = $('.lslide.active').index('.lslide');
  var index = parseInt(index)+1;
  var total = $('.overlay-control.photos a span.total')
  var current = $('.overlay-control.photos a span.current');
  total.html(size);
  current.html(index);
});

function loader(modal) {
  $('#loader').show();
  window.setTimeout(function () {
    $('#loader').hide();
    $(modal).modal('show');
  },200);
}

function likeProfile(id,is_encounter) {
  var heart = $("#heart");
  $.get(base+'/ajax/likeProfile.php?id='+id, function(data) {
    heart.html(data);
  });
  if(is_encounter == true) {
    newEncounter(id);
  } else {
    $('.encounter-controls .btn-primary').prop('disabled',false);
  }
}

function dislikeProfile(id,is_encounter) {
  var heart = $("#times");
  $.get(base+'/ajax/dislikeProfile.php?id='+id, function(data) {
    heart.html(data);
  });
  if(is_encounter == true) {
    newEncounter(id);
  } else {
    $('.encounter-controls .btn-danger').prop('disabled',false);
  }
}

function favoriteProfile(id) {
  var favorite = $("#favorite");
  $.get(base+'/ajax/favoriteProfile.php?id='+id, function(data) {
    favorite.html(data);
  });
}

function deletePhoto(photo_id) {
  $.get(base+'/ajax/deletePhoto.php?photo_id='+photo_id, function(data) {
    location.reload();
  });
}

function setAsProfilePhoto(photo_id) {
  $.get(base+'/ajax/setAsProfilePhoto.php?photo_id='+photo_id, function(data) {
    location.reload();
  });
}

function selectGift(id) {
  for (var i = 1; i <= 26; i++) {
    if(id != i) {
      $('#gift'+i).css('border','2px solid #FFF');
    } else {
      $('#gift'+i).css('border','2px solid #2B65F8');
    }
  };
  $('#giftValue').val(id);
}

function prevSlide() {
  var slide = $('.lslide.active').attr('data-src');
  var memory = $('#reported-photo');
  var index = $('span.current').html();
  var total = $('span.total').html();
  if(index == 0) {
    index = 1;
  }
  var index = parseInt(index);
  var total = parseInt(total);
  if(index > 1) {
    index = index-1;
  }
  console.log(index);
  var current = $('.overlay-control.photos a span.current');
  current.html(index);
  if(memory.length) {
   memory.val($('.lslide.active').attr('data-path'));
 }
}

function nextSlide() {
  var slide = $('.lslide.active').attr('data-src');
  var memory = $('#reported-photo');
  var index = $('span.current').html();
  var total = $('span.total').html();
  if(index == 0) {
    index = 1;
  }
  var index = parseInt(index);
  var total = parseInt(total);
  if(index < total) {
    index = index+1;
  }
  console.log(index);
  var current = $('.overlay-control.photos a span.current');
  current.html(index);
  if(memory.length) {
   memory.val($('.lslide.active').attr('data-path'));
 }
}

function newEncounter(id) {
  $.get(base+'/ajax/newEncounter.php', function(data) {
    $('.encounter').html(data);
    var encounters = $('#encounterGallery ul').lightSlider({
      gallery: false,
      prevHtml: '<i class="ti-angle-left encounter-control" onclick="prevSlide(); return false;"></i>',
      nextHtml: '<i class="ti-angle-right encounter-control" onclick="nextSlide(); return false;"></i>',
      item: 1,
      loop: false,
      thumbItem:9,
      slideMargin:0,
      enableDrag: false,
      currentPagerPosition:'left',
      controls: true,
      vertical: false,
      verticalHeight:640,
      vThumbWidth: 60,
      pager: false,
      mode: 'fade',
      onSliderLoad: function(el) {
        var slide = $('.lslide.active').attr('data-src');
        var memory = $('#reported-photo');
        if(memory.length) {
          memory.val($('.lslide.active').attr('data-path'));
        }
      }
    });
  });
}

function getSpotlight() {
  $.get(base+'/ajax/getSpotlight.php', function(data) {
    $('.spotlight').html(data);
    $('.spotlight-user').each(function() { 
      $(this).qtip({
        content: {
          text: $(this).next('div').html(),
        },
        position: {
          at: 'bottom center'
        },
        style: {
          corner: false,
          classes: ''
        },
      });
    });
  });
}

function toggleFilter() {
  $('.filter-area').slideToggle('fast');
}

function unlockEducation() {
  loader('#unlock-education');
}

function minimizeNav() {
  var width = $(document).width();
  if(width <= 1145) {
    $('#sidebar-wrapper').removeClass('animated fadeInLeft');
    $('#wrapper').css('padding-left','55px');
  }
}

function maximizeNav() {
  var width = $(document).width();
  if(width <= 1145) {
    $('#wrapper').css('padding-left','250px'); 
    $('#sidebar-wrapper').addClass('animated fadeInLeft');
  }
}

function showSingleAward(id,user_id,time) {
  $.get(base+'/ajax/awardModal.php?id='+id+'&user_id='+user_id+'&time='+time, function(data) {
    $('#single-award .modal-body').html(data);
  });
  loader('#single-award');
}

function showSingleGift(id,user_id) {
  $.get(base+'/ajax/giftModal.php?id='+id+'&user_id='+user_id, function(data) {
    $('#single-gift .modal-body').html(data);
  });
  loader('#single-gift');
}

function fbVerify() {
  FB.getLoginStatus(function(response) {
    if(response.status === 'connected') {
      $('.item').addClass('verified');
      console.log('Already connected');
    } else {
      FB.login(function(response){
        if(response.status === 'connected') {
         FB.api(
          '/me?fields=work,education,friends,likes',
          function (response) {
            if(response && !response.error) {
              var friends = response.friends.summary.total_count;
              $.post(base+'/ajax/fbVerify.php', { friends: friends })
              .done(function( data ) {
                $('.item').addClass('verified');
              });
            }
          }
          );
         console.log('Connected');
       } 
     },{scope: 'user_friends,user_education_history,user_work_history,user_likes'});
    }
  });
}

function fbSyncInfo(info) {
  if(info === 'work_and_education') {
    FB.getLoginStatus(function(response) {
      if(response.status === 'connected') {
       FB.api(
        '/me?fields=work,education,friends,likes',
        function (response) {
          if(response && !response.error) {
            var work = response.work[response.work.length-1].employer.name;
            var education = response.education[response.education.length-1].school.name;
            var friends = response.friends.summary.total_count;
            var likes = JSON.stringify(response.likes);
            console.log('Work: '+work);
            console.log('Education: '+education);
            console.log(friends+' Friends');
            console.log(likes);
            $.post(base+'/ajax/fbSyncInfo.php', { work: work, education: education, friends: friends, likes: likes })
            .done(function( data ) {
              location.reload();
            });
          }
        }
        );
     } else {
      FB.login(function(response){
        if(response.status === 'connected') {
         FB.api(
          '/me?fields=work,education,friends,likes',
          function (response) {
            if(response && !response.error) {
              var work = response.work[response.work.length-1].employer.name;
              var education = response.education[response.education.length-1].school.name;
              var friends = response.friends.summary.total_count;
              var likes = JSON.stringify(response.likes);
              console.log('Work: '+work);
              console.log('Education: '+education);
              console.log(friends+' Friends');
              console.log(likes);
              $.post(base+'/ajax/fbSyncInfo.php', { work: work, education: education, friends: friends, likes: likes })
              .done(function( data ) {
                location.reload();
              });
            }
          }
          );
       } 
     },{scope: 'user_friends,user_education_history,user_work_history,user_likes'});
}
});
}
}

function fbInvite() {
  FB.ui({
    method: 'share',
    href: 'http://getdatoo.com/index.php',
  }, function(response){
    if (response) {
      // successful invite
    } else {
      // failed to invite
    }
  });
}

function getPosition(position) {
  $.get(base+'/ajax/setPosition.php?longitude='+position.coords.longitude+'&latitude='+position.coords.latitude);
}

function toggleGallery() {
  if($('#galleryMin').text().length > 11) {
    $('#galleryFull').slideToggle('fast');
    $('#galleryMin').slideToggle('fast');
  }
}

function getGalleryFull(id,clicked) {
  if(clicked != 0) {
    clicked = $(clicked).parent();
  }
  if($('#galleryFull ul').is(':empty')) {
    $.get(base+'/ajax/getGalleryFull.php?id='+id, function(data) {
      galleryFull = $('#galleryFull ul').html(data);
      var galleryFull = $('#galleryFull ul').lightSlider({
        gallery: false,
        prevHtml: '<i class="ti-angle-left encounter-control" onclick="prevSlide(); return false;"></i>',
        nextHtml: '<i class="ti-angle-right encounter-control" onclick="nextSlide(); return false;"></i>',
        item: 1,
        loop: false,
        thumbItem:9,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        controls: true,
        vertical: false,
        verticalHeight:640,
        vThumbWidth: 60,
        pager: false,
        mode: 'fade',
        onSliderLoad: function(el) {
          var slide = $('.lslide.active').attr('data-src');
          var memory = $('#reported-photo');
          if(memory.length) {
            memory.val($('.lslide.active').attr('data-path'));
          }
        },
      });
      var size = $('.lslide').size();
      var index = $('.lslide.active').index('.lslide');
      var index = parseInt(index)+1;
      var total = $('.overlay-control.photos a span.total')
      var current = $('.overlay-control.photos a span.current');
      total.html(size);
      if(clicked != 0) {
        var index2 = $(clicked).index('.gallery-item');
        var index2 = parseInt(index2);
        current.html(index2+1);   
        console.log('Index: '+index2);
        galleryFull.goToSlide(index2);
      } else { 
        galleryFull.goToSlide(0);
      }
    });
}
var galleryFull = $('#galleryFull ul').lightSlider({
  gallery: false,
  prevHtml: '<i class="ti-angle-left encounter-control" onclick="prevSlide(); return false;"></i>',
  nextHtml: '<i class="ti-angle-right encounter-control" onclick="nextSlide(); return false;"></i>',
  item: 1,
  loop: false,
  thumbItem:9,
  slideMargin:0,
  enableDrag: false,
  currentPagerPosition:'left',
  controls: true,
  vertical: false,
  verticalHeight:640,
  vThumbWidth: 60,
  pager: false,
  mode: 'fade',
  onSliderLoad: function(el) {
    var slide = $('.lslide.active').attr('data-src');
    var memory = $('#reported-photo');
    if(memory.length) {
      memory.val($('.lslide.active').attr('data-path'));
    }
  },
});
if(clicked != 0) {
  var index = $(clicked).index('.gallery-item');
  var index = parseInt(index);
  var current = $('.overlay-control.photos a span.current');
  current.html(index+1); 
  console.log('Index: '+index);
  galleryFull.goToSlide(index);
} 
}

function reportPhoto() {
  loader('#report-photo');
}

function toggleReportComment(radio) {
  if(radio.value == 2 || radio.value == 3) {
    $('#report-comment').show();
    $('#report-comment textarea').attr('required');
  } else {
    $('#report-comment').hide(); 
  }
}

$('#take-photo-form input').change(function(e) {
  e.preventDefault()
  var form = $('#take-photo-form');
  var formData = new FormData(form[0]);
  console.log(formData);
  $.ajax({
    type:'POST',
    url: $(form).attr('action'),
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    success:function(data){
      $('#take-photo .modal-body').html(data);
    },
    error: function(data){
      console.log("error");
      console.log(data);
    }
  });
});

function makeEditable(num) {
  $('#uneditable-'+num).toggle();
  $('#editable-'+num).toggle();
}

function toggleMap() {
  if($('.map').is(':visible')) {
    $('.profile-section-heading .active').hide();
    $('.profile-section-heading .inactive').show();
  } else {
    $('.profile-section-heading .active').show();
    $('.profile-section-heading .inactive').hide();
  }
  $('.map').slideToggle('fast');
}

function chatContacts(id) {
  var last;
  if(id == 0) {
    last = true;
  } else {
    last = false;
  }
  $.get(base+'/ajax/chatContacts.php', function(data) {
    if(data.length > 0) {
      $('.chats-list').html(data);
      $('.chats-list').Emoji({
        path: base+'/img/emoji/apple40/',
        ext: 'png'
      });
      if(last == true) {
        var last_user = $('.chats-list').children().first().data('id');
        getMessages(last_user);
        $('#chat-open'+last_user).addClass('active');
      } else {
        getMessages(id);
      }
    } else {
      // No conversations to show
      alert('No conversations to show');
    }
  });
}

function getMessages(id) {
  $.get(base+'/ajax/getMessages.php?id='+id, function(data) {
    if(data.length > 0) {
      var result = JSON.parse(data);
      var receiver = result.receiver;
      $('.chat-top-right').html(receiver);
      $('.chat-sidebar-user').each(function() {
        if($(this).attr('id') == 'chat-open-'+id) {
          $(this).addClass('active');
        } else {
          $(this).removeClass('active');
        }
      });
      $('.chat-content').html(result.messages);
      $('.chat-content-text').each(function() {
        $(this).Emoji({
          path: base+'/img/emoji/apple40/',
          ext: 'png'
        });;
      });
      $('.chat-content-wrap').mCustomScrollbar({
        theme: 'light-3',
        live: 'on',
      });
      $('.chat-content-wrap').mCustomScrollbar('update');
      $('.chat-content-wrap').mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
    } 
  });
  getStickerPacks(id);
}

function appendToMessage(str) {
  var message = $('#message');
  var alt = $(str).children('img').attr('alt');
  console.log(alt);
  message.val(message.val()+alt+' ');
}

function sendMessage() {
  var receiver_id = $('.chat-sidebar-user.active').parent().data('id');
  if(isNaN(receiver_id) == true) {
    var receiver_id = $('#receiver_id').val();
  }
  var message = $('#message');
  if(message.val() != '' && message.val() != ' ') {
    $.get(base+'/ajax/sendMessage.php?receiver_id='+receiver_id+'&msg='+encodeURIComponent(message.val()), function(data) {
      getMessages(receiver_id);
      $('.chat-content-wrap').mCustomScrollbar('update');
      $('.chat-content-wrap').mCustomScrollbar('scrollTo','bottom',{scrollInertia:0});
      message.val('');
    });
  }
}

function loadEmojis() {
  $.get(base+'/ajax/getEmoji.php', function(data) {
    $('.emoji-content').html(data);
    $('.emoticon').each(function() {
      $(this).Emoji({
        path: base+'/img/emoji/apple40/',
        ext: 'png'
      });
    });
  });
}

function toggleEmojiMenu() {
  $('.gift-menu').hide();
  var emoji_menu = $('.emoji-menu');
  if(emoji_menu.is(':visible')) {
    $('.chat-content-wrapper').mCustomScrollbar('update');
  } else {
    $('.chat-content-wrapper').mCustomScrollbar('disable');
  }
  $('.emoji-top-link').addClass('active');
  emoji_menu.slideToggle('slow');
  loadEmojis();
  setActiveEmojiLink('.emoji-top-link');
  $('.emoji-content-wrap').mCustomScrollbar({
    theme: 'light-3',
    live: 'on',
  });
  $('.emoji-content-wrap').mCustomScrollbar('update');
}

function setActiveEmojiLink(link) {
  $('.emoji-top-img').each(function() {
    $(this).removeClass('active');
  });
  $('.emoji-top-link').removeClass('active');
  $(link).addClass('active');
}

function getStickerPacks(receiver_id) {
  $.get(base+'/ajax/getStickerPacks.php?receiver_id='+receiver_id, function(data) {
    $('.emoji-sticker-packs').html(data);
  });
}

function loadStickers(pack_id,receiver_id,is_premium) {
  $.get(base+'/ajax/getStickers.php?pack_id='+pack_id+'&receiver_id='+receiver_id+'&is_premium='+is_premium, function(data) {
    $('.emoji-content').html(data);
  });
}

function unlockStickers(pack_id,receiver_id,is_premium) {
  document.getElementById('darkLayer-'+pack_id).style.display = 'none';
  $.get(base+'/ajax/unlockStickerPack.php?pack_id='+pack_id, function(data) {
    loadStickers(pack_id,receiver_id,is_premium);
  });
}

function sendSticker(sticker_id,receiver_id) {
  $.get(base+'/ajax/sendSticker.php?sticker_id='+sticker_id+'&receiver_id='+receiver_id);
  $('.chat-content-wrap').mCustomScrollbar('update');
  $('.chat-content-wrap').mCustomScrollbar('scrollTo','bottom',{scrollInertia:0});
  getMessages(receiver_id);
}

function initChat(user) {
  chatContacts(user);
  loader('#messages');
}

function deleteMessages(receiver_id) {
 $.get(base+'/ajax/deleteMessages.php?receiver_id='+receiver_id, function(data) {
  location.reload();
});
}

function changeGateway(gateway,button) {
  $('.payment-gateways .gateway').each(function() {
    $(this).removeClass('active');
  });
  $(button).addClass('active');
  $('.gateway-process').each(function() {
    if(gateway == 'fortumo') {
      $('.title').hide();
      $('.description').hide();
      $('.amount').hide();
      $('.payment-gateways').css('height','550px');
    } else {
      $('.title').show();
      $('.description').show();
      $('.amount').show();
      $('.payment-gateways').css('height','400px');

    }
    if($(this).attr('id') != gateway) {
      $(this).hide();
    } else {
      $(this).show();
    }
  });
}

function changeAmount(amount) {
  var amount = $(amount).val();
  $('input[name=credits]').each(function() {
    $(this).val(amount);
  });
}

function changeDuration(duration) {
  var duration = $(duration).val();
  $('input[name=duration]').each(function() {
    $(this).val(duration);
  });
}

function creditAction(action) {
  $.get(base+'/ajax/creditAction.php?action='+action, function(data) {
    location.reload();
  });
}

window.open = function (open) {
  return function (url, name, features) {
    var w = 475;
    var h = 183;
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var override_features = 'width=475,height=183,left=' + left + ',top=' + top + ',scrollbars=1,location=1,toolbar=0';
    return open.call(window, url, name, override_features);
  };
}(window.open);

$('#message').keypress(function (e) {
 var key = e.which;
 if(key == 13) {
    sendMessage();
  }
});   

$('#add-photos').on('shown.bs.modal', function (e) {
  var myDropzone = new Dropzone('div#myDropzone', { 
    acceptedFiles: 'image/*',
    url: base+'/ajax/addPhotos.php',
    paramName: 'file', 
    maxFilesize: 10, 
    parallelUploads: 2,
    thumbnailWidth: 150
  });
  myDropzone.on('queuecomplete', function(file, res) {
    location.reload();
  });
});

$('.location-autocomplete').focus(function() {
  $('.tp-ac__list').show();
});

$('.location-autocomplete').focusout(function() {
  $('.tp-ac__list').hide();
});

$(document).on('show.bs.modal', '.modal', function () {
  var zIndex = 1040 + (10 * $('.modal:visible').length);
  $(this).css('z-index', zIndex);
  setTimeout(function() {
    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
  }, 0);
});
var createAccount = new function () {

    this.models = {
        btnCreateAccount: "#btn-create-user",
        inputUrl: "#url",
        formCreateUser: "#form-create-user",

        // Value input
        first_name: "first_name",
        email: "email",
        birth_day: "birth_day",
        birth_month: "birth_month",
        birth_year: "birth_year",
        gender: "gender",
        here_to: "here_to",
        latitude : "latitude",
        longitude : "longitude",
        city : "city",
        country : "country",

        // Span error message
        error_name: "#error-name",
        error_email: "#error-email",
        error_location: "#error-location",
        error_password: "#error-password",
        error_birthday: "#error-birth-day",

        // Container
        container: ".inner-container"
    };

    this.init = function () {
        $(createAccount.models.btnCreateAccount).click(function () {
            var checkValidate = true;
            checkValidate = createAccount.events.ajaxCreateUser();

            if (!checkValidate) {
                createAccount.events.setFormInput();
                return false;
            }
        });
    };

    this.events = {
        ajaxCreateUser: function () {
            // get url
            var url,
                    first_name,
                    email,
                    birth_day,
                    birth_month,
                    birth_year,
                    gender,
                    here_to,
                    error_name,
                    error_email,
                    error_location,
                    error_password,
                    error_birthday,
                    pass,
                    latitude,
                    longitude,
                    city,
                    country;

            var email_regExp = new RegExp(/^([A-Z0-9.%+-]+@@[A-Z0-9.-]+.[A-Z]{2,6})*([,;][\s]*([A-Z0-9.%+-]+@@[A-Z0-9.-]+.[A-Z]{2,6}))*$/i);

            url = $(createAccount.models.inputUrl).val();

            // Input value
            first_name = $("input[name='first_name']").val();
            email = $("input[name='email']").val();
            birth_day = $("#birth-day").find(":selected").text();
            birth_month = $("#birth-month").find(":selected").val();
            birth_year = $("#birth-year").find(":selected").text();
            gender = $("input[name='gender']").val();
            here_to = $("input[name='here_to']").val();
            pass = $("input[name='password']").val();
            latitude =  $("input[name='latitude']").val();
            longitude =  $("input[name='longitude']").val();
            city =  $("input[name='city']").val();
            country =  $("input[name='country']").val();

            // Span error message
            error_name = $(createAccount.models.error_name);
            error_email = $(createAccount.models.error_email);
            error_location = $(createAccount.models.error_location);
            error_password = $(createAccount.models.error_password);
            error_birthday = $(createAccount.models.error_birthday);

            // Message error
            var message_name,
                    message_email,
                    message_pass,
                    size_content,
                    message_format,
                    message_birthday;

            var isName,
                    isEmail,
                    isPass,
                    isBirthDay;

            isName = true;
            isEmail = true;
            isPass = true;
            isBirthDay = true;

            message_name = "* Please enter your name";
            message_email = "* Please enter your email";
            message_format = "* Please enter valid email";
            message_pass = "* Please enter your password";
            message_birthday = "* Please enter your birthday";

            size_content = 520;

            // Check null input
            // Name
            if (!$.trim(first_name).length) {
                error_name.show();
                error_name.html(message_name);
                $("input[name='first_name']").addClass('border-input-error');
                isName = false;
            } else {
                if ($.trim(first_name).length > 30) {
                    message_name = "* Please enter your name 1 - 30 character";
                    error_name.show();
                    error_name.html(message_name);
                    $("input[name='first_name']").addClass('border-input-error');
                    isName = false;
                } else {
                    error_name.hide();
                    $("input[name='first_name']").removeClass('border-input-error');  
                }
            }

            // Email
            if (!$.trim(email).length) {
                error_email.show();
                error_email.html(message_email);
                $("input[name='email']").addClass('border-input-error');
                size_content = size_content + 20;
                isEmail = false;
            } else {
                if (!createAccount.events.isValidEmailAddress(email)) {
                    error_email.show();
                    error_email.html(message_format);
                    $("input[name='email']").addClass('border-input-error');
                    size_content = size_content + 20;
                    isEmail = false;
                } else if ($.trim(email).length > 100) {
                    message_format = "* Please enter your email 1 - 100 character";
                    error_email.show();
                    error_email.html(message_format);
                    $("input[name='email']").addClass('border-input-error');
                    size_content = size_content + 20;
                    isEmail = false;
                } else {
                    error_email.hide();
                    $("input[name='email']").removeClass('border-input-error');
                }
            }

            // Pass
            if (!$.trim(pass).length) {
                error_password.show();
                error_password.html(message_pass);
                $("input[name='password']").addClass('border-input-error');
                size_content = size_content + 20;
                isPass = false;
            } else {
                if ($.trim(pass).length > 123) {
                    message_pass = "* Please enter your password 1 - 123 character";
                    error_password.show();
                    error_password.html(message_pass);
                    $("input[name='password']").addClass('border-input-error');
                    size_content = size_content + 20;
                    isPass = false;
                } else {
                    error_password.hide();
                    $("input[name='password']").removeClass('border-input-error');
                }
            }

            // Birth day
            if (!$.isNumeric(birth_day) || !$.isNumeric(birth_month) || !$.isNumeric(birth_year)) {
                error_birthday.show();
                error_birthday.html(message_birthday);
                //$("input[name='password']").addClass('border-input-error');
                size_content = size_content + 20;
                isBirthDay = false;
            } else {
                error_birthday.hide();
            }

            if (    !isName ||
                    !isEmail ||
                    !isPass ||
                    !isBirthDay
            )
            {
                $(createAccount.models.container).css({'height': size_content + "px"});
                return false;
            }
            
            $.ajax({
                type: "POST",
                url: url + '/create_account.php',
                dataType: 'json',
                data: {
                    create_account: "create_account",
                    first_name: first_name,
                    email: email,
                    birth_day: birth_day,
                    birth_month: birth_month,
                    birth_year: birth_year,
                    gender: gender,
                    here_to: here_to,
                    password: pass,
                    latitude : latitude,
                    longitude : longitude,
                    city : city,
                    country : country
                }
            }).done(function (data) {
                
                var isEmail;
                
                isEmail = true;
                
                if (data['error_message'] != null && data['type_message'] == "email") {
                    error_email.show();
                    error_email.html(data['error_message']);
                    $("input[name='email']").addClass('border-input-error');
                    size_content = size_content + 20;
                    $(createAccount.models.container).css({'height': size_content + "px"});
                    isEmail = false;
                } 
                
                if (data['error_message'] != null && data['type_message'] == "name") {
                    error_name.show();
                    error_name.html(data['error_message']);
                    $("input[name='first_name']").addClass('border-input-error');
                    size_content = size_content + 20;
                    $(createAccount.models.container).css({'height': size_content + "px"});
                    isName = false;
                }
                
                if (data['error_message'] != null && data['type_message'] == "pass") {
                    error_password.show();
                    error_password.html(data['error_message']);
                    $("input[name='password']").addClass('border-input-error');
                    size_content = size_content + 20;
                    $(createAccount.models.container).css({'height': size_content + "px"});
                    isPass = false;
                }
                
                if (    
                    !isName ||
                    !isEmail ||
                    !isPass 
                )
                {
                    return false;
                }
                
                window.location = url + '/encounters';
                return true;
                
            }).fail(function (error) {
                console.log(error);
            });
        },

        setFormInput: function () {
            $('#step-' + 3).fadeIn(400);
            $('#step-2').hide();
            $('#step-1').hide();
            $('#steps div:nth-of-type(3)').addClass('active');
            $('#steps div:nth-of-type(2)').removeClass('active');
            $('#steps div:nth-of-type(1)').removeClass('active');
            return false;
        },

        isValidEmailAddress: function (emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        },
    };
};

$(document).ready(function () {
    createAccount.init();
});
 $(document).ready(function() {
 	$('.select2').each(function(){
 		$(this).select2({minimumResultsForSearch: -1});
 	});
 	navigator.geolocation.getCurrentPosition(getPosition);
 	function getPosition(position) {
 		var latitude = position.coords.latitude;
 		var longitude = position.coords.longitude;
 		$('<input>').attr({
 			type: 'hidden',
 			name: 'latitude',
 			value: latitude
 		}).appendTo('form');
 		$('<input>').attr({
 			type: 'hidden',
 			name: 'longitude',
 			value: longitude
 		}).appendTo('form');
 	}
 });
 function nextStep(step,selected) {
 	var selected = $(selected).attr('data-value');
 	if(step == 2) {
 		$('#step-'+step).fadeIn(400);
 		$('#step-1').hide();
 		$('#steps div:nth-of-type(2)').addClass('active');
 		$('#steps div:nth-of-type(1)').removeClass('active');
 		$('#gender').val(selected);
 	}
 	if(step == 3) {
 		$('#step-'+step).fadeIn(400);
 		$('#step-2').hide();
 		$('#steps div:nth-of-type(3)').addClass('active');
 		$('#steps div:nth-of-type(2)').removeClass('active');
 		$('#here_to').val(selected);
 	}
 }
 function fbLogin() {
 	FB.getLoginStatus(function(response) {
 		if(response.status === 'connected') {
 			FB.api(
 				'/me?fields=id,name,email,work,education,friends,likes',
 				function (response) {
 					if(response && !response.error) {
 						var work = response.work[response.work.length-1].employer.name;
 						var education = response.education[response.education.length-1].school.name;
 						var friends = response.friends.summary.total_count;
 						var likes = JSON.stringify(response.likes);
 						var name = response.name;
 						var id = response.id;
 						var latitude = $('input[name=latitude]').val();
 						var longitude = $('input[name=longitude]').val();
 						console.log('ID: '+id);
 						console.log('Name: '+name);
 						console.log('Work: '+work);
 						console.log('Education: '+education);
 						console.log(friends+' Friends');
 						console.log(likes);
 						$.post(base+'/ajax/landing/fbLogin.php', { id: id, name: name, work: work, education: education, friends: friends, likes: likes, latitude: latitude, longitude: longitude})
 						.done(function( data ) {
 							if(data == 'true') {
 								location.href = base+'/encounters';
 							} else {
 								location.reload();
 							}
 						});
 					}
 				}
 				);
} else {
	FB.login(function(response){
		if(response.status === 'connected') {
			FB.api(
				'/me?fields=id,name,email,work,education,friends,likes',
				function (response) {
					if(response && !response.error) {
						var work = response.work[response.work.length-1].employer.name;
						var education = response.education[response.education.length-1].school.name;
						var friends = response.friends.summary.total_count;
						var likes = JSON.stringify(response.likes);
						var name = response.name;
						var id = response.id;
						var latitude = $('input[name=latitude]').val();
						var longitude = $('input[name=longitude]').val();
						console.log('ID: '+id);
						console.log('Name: '+name);
						console.log('Work: '+work);
						console.log('Education: '+education);
						console.log(friends+' Friends');
						console.log(likes);
						$.post(base+'/ajax/landing/fbLogin.php', { id: id, name: name, work: work, education: education, friends: friends, likes: likes, latitude: latitude, longitude: longitude})
						.done(function( data ) {
							if(data == 'true') {
								location.href = base+'/encounters';
							} else {
								location.reload();
							}
						});
					}
				}
				);
} 
},{scope: 'user_friends,user_education_history,user_work_history,user_likes'});
}
});
}
function selectLanguage(language) {
	var language = $(language).val();
	$.get(base+'/ajax/landing/selectLanguage.php?language='+language, function(data) {
		location.reload();
	});
}
window.open = function (open) {
	return function (url, name, features) {
		var w = 475;
		var h = 183;
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var override_features = 'width=475,height=183,left=' + left + ',top=' + top + ',scrollbars=1,location=1,toolbar=0';
		return open.call(window, url, name, override_features);
	};
}(window.open);
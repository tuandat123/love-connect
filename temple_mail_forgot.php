<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>A Simple Responsive HTML Email</title>
      <style type="text/css">
         body {margin: 0; padding: 0; min-width: 100%!important;}
      </style>
   </head>
   <body yahoo bgcolor="#f6f8f1">
      <div lang="vi" dir="ltr" bgcolor="#f2f2f2">
         <table class="m_-2223337181558588676page" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#f2f2f2" style="direction:ltr;min-width:320px;text-align:center;color:#292c2e;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Arial,sans-serif">
            <tbody>
               <tr>
                  <td class="m_-2223337181558588676page__head m_-2223337181558588676page_head" style="padding:20px 0">
                   
                  </td>
               </tr>
               <tr>
                  <td class="m_-2223337181558588676wraper" style="padding:0 20px;text-align:center">
                     <span style="display:inline-block;vertical-align:top;width:100%;max-width:640px;text-align:left">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:640px;min-width:280px;margin:0 auto">
                           <tbody>
                              <tr>
                                 <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-2223337181558588676no-tips">
                                       <tbody>
                                          <tr>
                                             <td style="text-align:center;color:#333844;font-size:16px" class="m_-2223337181558588676main">
                                                <table class="m_-2223337181558588676section m_-2223337181558588676section--last" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td class="m_-2223337181558588676section__promo" style="padding:8% 5% 0;text-align:center">
                                                            <span style="display:inline-block;vertical-align:top">
                                                               <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:480px;margin:0 auto">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td align="center">
                                                                            <img src="{Url_Image_Logo}" class="m_-2223337181558588676app__logo CToWUd" height="50" alt="" border="0">
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </span>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td class="m_-2223337181558588676section__text" style="padding:5% 5% 3%;text-align:center">
                                                            <span class="m_-2223337181558588676section-text" style="font-size:16px;line-height:22px;color:#7f7f7f;display:inline-block;vertical-align:top;max-width:470px">
                                                                {title_mail}
                                                            </span>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td class="m_-2223337181558588676section__footer" style="padding:0 0 8%;text-align:center">
                                                            <a href="{Link_Url_Reset_Pass}" class="m_-2223337181558588676btn" style="display:inline-block;padding:13px 30px;background:rgba(0, 0, 0, 0) linear-gradient(to right, #0079fc, #1fd9e6) repeat scroll 0 0;text-decoration:none;border-radius:8px;color:#fff;font-weight:600;font-size:16px;text-align:center" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://us1.badoo.com/access.phtml?UID%3D1451302965%26secret%3DIzT1pTDEUL%26m%3D2%26g%3D7-0-4%26pref_lang%3D78%26email%3Ddatpdt%2540rikkeisoft.com%26mid%3D0301fc28e4ff9d0d5400000002568120350000000000014e4d0f&amp;source=gmail&amp;ust=1517459524573000&amp;usg=AFQjCNF1BvZAGFBn7uOBgKgpKtKPQvtAwQ">
                                                            {Button_Temple_Mail}
                                                            </a>
                                                            <div class="m_-2223337181558588676hint" style="padding:20px 5% 0;color:#949494;font-size:13px;line-height:18px">
                                                               {Description_Temple_Mail}
                                                            </div>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <table bgcolor="#ffffff" class="m_-2223337181558588676section m_-2223337181558588676section--bottom" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td style="text-align:center;padding:25px 10%;border-top:1px solid #e5e5e5">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="max-width:400px;margin:0 auto">
                                                               <tbody>
                                                                  <tr>
                                                                     <td class="m_-2223337181558588676app" style="padding:0 10px;width:50%;vertical-align:top;text-align:center">
                                                                        <a href="#" style="display:inline-block;text-decoration:none;color:#000;text-align:left;vertical-align:middle;margin:0 auto" target="_blank" data-saferedirecturl="">
                                                                           <div style="text-align:center">
                                                                              <img src="{Url_Image_Google_Play}" class="m_-2223337181558588676app__logo CToWUd" height="32" alt="" border="0">
                                                                           </div>
                                                                           <span class="m_-2223337181558588676app__txt" style="font-size:14px;display:block;border-top:8px solid transparent;color:#6a6a6a;text-align:center"><span class="m_-2223337181558588676app__subtxt" style="font-size:12px;line-height:12px">{Dowload_App_Google_Play}</span><br> Google Play</span>
                                                                        </a>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td style="padding:0 0 4%">
                                                            <span class="m_-2223337181558588676social-links">
                                                               <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td class="m_-2223337181558588676social-links__inner m_-2223337181558588676social-links_inner" style="text-align:center;padding:25px 0 0">
                                                                           <div style="color:#000;display:block;font-size:14px;border-bottom:10px solid transparent">{Follow_FaceBook}</div>
                                                                           <a href="#" target="_blank" data-saferedirecturl=""><img src="{Url_Image_FaceBook}" width="42" height="42" alt="Facebook" style="vertical-align:middle" border="0" class="CToWUd"></a>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </span>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </span>
                  </td>
               </tr>
            </tbody>
         </table>
         <div class="yj6qo"></div>
         <div class="adL">
         </div>
      </div>
   </body>
</html>
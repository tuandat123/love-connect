<?php
session_set_cookie_params(172800);
session_start();
require('../../core/classes.php');
require('../../core/paypal/src/PaypalIPN.php');
$system = new Core;
$db = $system->db();

use overint\PaypalIPN;

$ipn = new PaypalIPN();
$ipn->useSandbox();
$verified = $ipn->verifyIPN();

if($verified) {

    $custom = json_decode($_POST['custom'],1);
    $type = $custom['type'];

    if($type == 'credits') {
        $credits = $custom['credits'];
        $query = $db->prepare("UPDATE users SET credits=credits+".$credits." WHERE id = ?");
    } elseif($type == 'superpowers') {
        $exp = $custom['exp'];
        $query = $db->prepare("UPDATE users SET has_superpowers='1',superpowers_expiration='".$exp."' WHERE id = ?");
    }
    
    $query->bind_param('s', $my_user->id);
    $query->execute();

}
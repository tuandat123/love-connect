<?php
session_set_cookie_params(172800);
session_start();
require('../../core/classes.php');
require('../../core/paypal/src/PaypalIPN.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

if(isset($_POST['type'])) {

	$type = $_POST['type'];

	if($type == 'credits') {

		$credits = $_POST['credits'];

		switch ($credits) {
			case 500:
			$price = $system->setting('credits_price_500');
			break;
			case 1000:
			$price = $system->setting('credits_price_1000');
			break;
			case 2000:
			$price = $system->setting('credits_price_2000');
			break;
		}

		$query = array();
		$query['notify_url'] = $system->getDomain().'/payment/paypal/ipn.php';
		$query['cmd'] = '_xclick';
		$query['business'] = $system->setting('paypal_email');
		$query['currency_code'] = strtoupper($system->setting('currency'));
		$query['custom'] = json_encode(array('user_id' => $my_user->id, 'type' => 'credits', 'credits' => $credits));
		$query['return'] = $system->getDomain().'/credits';
		$query['item_name'] = $system->setting('website_name').' Credits';
		$query['quantity'] = 1;
		$query['amount'] = $price;
		$query_string = http_build_query($query);
		header('Location: https://www.paypal.com/cgi-bin/webscr?' . $query_string);

	} elseif($type == 'superpowers') {

		$duration = $_POST['duration'];

		switch ($duration) {
			case 1:
			$exp = strtotime('+7 days');
			$price = $system->setting('superpowers_1_week');
			break;
			case 2:
			$exp = strtotime('+1 month');
			$price = $system->setting('superpowers_1_month');
			break;
			case 3:
			$exp = strtotime('+3 months');
			$price = $system->setting('superpowers_3_months');
			break;
		}

		$query = array();
		$query['notify_url'] = $system->getDomain().'/payment/paypal/ipn.php';
		$query['cmd'] = '_xclick';
		$query['business'] = $system->setting('paypal_email');
		$query['currency_code'] = strtoupper($system->setting('currency'));
		$query['custom'] = json_encode(array('user_id' => $my_user->id, 'type' => 'superpowers', 'exp' => $exp));
		$query['return'] = $system->getDomain().'/superpowers';
		$query['item_name'] = $system->setting('website_name').' Super Powers';
		$query['quantity'] = 1;
		$query['amount'] = $price;
		$query_string = http_build_query($query);
		header('Location: https://www.paypal.com/cgi-bin/webscr?' . $query_string);

	}
}
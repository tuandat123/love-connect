<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
require('../core/stripe/init.php');
$system = new Core;
$my_user = new User($_SESSION['user_id']);
$system->getLanguage();
$db = $system->db();

if(isset($_POST['stripeToken'])) {

	$stripe_token = $_POST['stripeToken'];

	\Stripe\Stripe::setApiKey($system->setting('stripe_secret_key'));

	if(isset($_POST['type'])) {

		$type = $_POST['type'];

		if($type == 'credits') {

			$credits = $_POST['credits'];

			switch ($credits) {
				case 500:
				$price = $system->setting('credits_price_500');
				break;
				case 1000:
				$price = $system->setting('credits_price_1000');
				break;
				case 2000:
				$price = $system->setting('credits_price_2000');
				break;
			}

			try {

				$charge = \Stripe\Charge::create(array(
					'amount' => $price*100, 
					'currency' => strtolower($system->setting('currency')),
					'source' => $stripe_token,
					'description' => $system->setting('website_name').' Credits')
				);

				$query = $db->prepare("UPDATE users SET credits = credits + ? WHERE id = ?");
				$query->bind_param('ss', $credits, $my_user->id);
				$query->execute();

				header('Location: '.$system->getDomain().'/credits');
				exit;

			} catch(\Stripe\Error\Card $e) {

				// Payment failed

			}

		} elseif($type == 'superpowers') {

			$duration = $_POST['duration'];

			switch ($duration) {
				case 1:
				$exp = strtotime('+7 days');
				$price = $system->setting('superpowers_1_week');
				break;
				case 2:
				$exp = strtotime('+1 month');
				$price = $system->setting('superpowers_1_month');
				break;
				case 3:
				$exp = strtotime('+3 months');
				$price = $system->setting('superpowers_3_months');
				break;
			}

			try {

				$charge = \Stripe\Charge::create(array(
					'amount' => $price*100, 
					'currency' => strtolower($system->setting('currency')),
					'source' => $stripe_token,
					'description' => $system->setting('website_name').' Super Powers')
				);

				$query = $db->prepare("UPDATE users SET has_superpowers='1',superpowers_expiration='".$exp."' WHERE id = ?");
				$query->bind_param('s', $my_user->id);
				$query->execute();

				header('Location: '.$system->getDomain().'/superpowers');
				exit;

			} catch(\Stripe\Error\Card $e) {

				// Payment failed

			}
		}
	}
}
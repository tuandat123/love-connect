<?php
session_set_cookie_params(172800);
session_start();
require('../core/classes.php');
$system = new Core;
$db = $system->db();

function check_signature($params_array, $secret) {
	ksort($params_array);

	$str = '';
	foreach ($params_array as $k=>$v) {
		if($k != 'sig') {
			$str .= "$k=$v";
		}
	}
	$str .= $secret;
	$signature = md5($str);

	return ($params_array['sig'] == $signature);
}

if(!in_array($_SERVER['REMOTE_ADDR'],
	array('1.2.3.4', '2.3.4.5'))) {
	header('HTTP/1.0 403 Forbidden');
die('Error: Unknown IP');
}

$secret = '5fa58baab61a6091e587077987f6cba3';

if(empty($secret) || !check_signature($_GET, $secret)) {
 header('HTTP/1.0 404 Not Found');
 die('Error: Invalid signature');
}

if(isset($_GET['credit_name'])) {
  $type = 'credits';
} else {
  $type = 'superpowers';
}

if(preg_match("/completed/i", $_GET['status'])) {
  $sender = $_GET['sender']; // phone num.
  $cuid = $_GET['cuid']; // resource i.e. user
  $payment_id = $_GET['payment_id']; // unique id

  if($type == 'credits') {

  $amount = $_GET['amount']; // credits
  $query = $db->prepare("UPDATE users SET credits=credits+".$amount." WHERE id = ?");

} elseif($type == 'superpowers') {

  $exp = strtotime('+7 days'); // expiration
  $query = $db->prepare("UPDATE users SET has_superpowers='1',superpowers_expiration='".$exp."' WHERE id = ?");

}

$query->bind_param('s', $cuid);
$query->execute();

}
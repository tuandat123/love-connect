<?php

class Database {

    public $db;

    public function connectDatabase() {
        include($this->getAbsPath() . '/core/config.php');
        $this->domain = $domain;
        $this->db = new mysqli($db['host'], $db['user'], $db['pass'], $db['name']);
        $this->db->query('SET NAMES UTF8MB4');
    }

    public function db() {
        return $this->db;
    }

}

class Core extends Database {

    public $domain;
    public $settings;
    public $lang;

    function __construct() {
        $this->connectDatabase();
        $this->getSettings();
        $this->getLanguage();
    }

    public function getDomain() {
        return $this->domain;
    }

    public function getSettings() {
        $settings = $this->db->query("SELECT * FROM settings");
        $settings = $settings->fetch_object();
        $this->settings = $settings;
    }

    public function getAbsPath() {
        return dirname(dirname(__FILE__));
    }

    public function getLanguage() {
        if (isset($_SESSION['language'])) {
            $language = $_SESSION['language'];
        } else {
            $language = 'english';
            $_SESSION['language'] = $language;
        }
        require($this->getAbsPath() . '/languages/' . $language . '/language.php');
        $this->lang = $lang;
    }

    public function translate($str) {
        return $this->lang[$str];
    }

    public function setting($var) {
        return $this->settings->$var;
    }

}

class Auth extends Core {

    public function isLogged() {
        if (!empty($_SESSION)) {
            if (isset($_SESSION['auth'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function isAdmin() {
        if (!empty($_SESSION)) {
            if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function hashPassword($password) {
        return hash('sha256', $password);
    }

}

class Geo extends Core {

    public function coordsToKm($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
                pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
        $angle = atan2(sqrt($a), $b);
        return ($angle * $earthRadius) / 1000;
    }

    public function reverseGeo() {
        $json = file_get_contents('https://www.geoip-db.com/json');
        return json_decode($json);
    }

}

class Helper extends Core {

    public function timeAgo($ptime) {
        $etime = time() - $ptime;
        if ($etime < 1) {
            return $this->translate('just_now');
        }
        $a = array(365 * 24 * 60 * 60 => $this->translate('year'),
            30 * 24 * 60 * 60 => $this->translate('month'),
            24 * 60 * 60 => $this->translate('day'),
            60 * 60 => $this->translate('hour'),
            60 => $this->translate('minute'),
            1 => $this->translate('second')
        );
        $a_plural = array('year' => $this->translate('years'),
            'month' => $this->translate('months'),
            'day' => $this->translate('days'),
            'hour' => $this->translate('hours'),
            'minute' => $this->translate('minutes'),
            'second' => $this->translate('seconds')
        );
        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[strtolower($str)] : $str) . ' ' . $this->translate('ago');
            }
        }
    }

    public function emptyText($show, $text) {
        if (empty($text)) {
            echo $show;
        } else {
            echo $text;
        }
    }

}

class EncodeMail extends Core {

    /**
     * Returns an encrypted & utf8-encoded
     */
    public function encrypt($pure_string, $encryption_key) {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
        return $encrypted_string;
    }

    /**
     * Returns decrypted original string
     */
    public function decrypt($encrypted_string, $encryption_key) {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
        return $decrypted_string;
    }

}

class Widget extends Core {

    public function inviteFriends() {
        $invite_friends = $this->db->query("SELECT * FROM users WHERE profile_picture != '' ORDER BY RAND() LIMIT 3");
        include($this->getAbsPath() . '/layout/widgets/invite_friends.phtml');
    }

    public function footerPages() {
        $pages = $this->db->query("SELECT * FROM pages ORDER BY id ASC");
        include($this->getAbsPath() . '/layout/widgets/footer_pages.phtml');
    }

}

class User extends Geo {

    function __construct($user_id) {
        parent::__construct();
        $this->getLanguage();
        $user = $this->db->query("SELECT * FROM users WHERE id = " . $user_id . "");
        if ($user->num_rows != 0) {
            $user = $user->fetch_object();
            $vars = get_object_vars($user);
            foreach ($vars as $var => $value) {
                $this->$var = $value;
            }
        }
    }

    public function markActivity() {
        $this->db->query("UPDATE users SET last_active = '" . time() . "' WHERE id = " . $this->id . "");
    }

    public function markEncounter($encounter_id) {
        $this->db->query("UPDATE users SET last_encounter = " . $encounter_id . " WHERE id = " . $this->id . "");
    }

    public function isOnline() {
        if (time() - $this->last_active <= 300) {
            return true;
        } else {
            return false;
        }
    }

    public function isVerified() {
        if ($this->is_verified == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function hasSuperPowers() {
        if ($this->has_superpowers == 1 || $this->superpowers_expiration > time()) {
            return true;
        } else {
            return false;
        }
    }

    public function isPromoted($type) {
        if ($type == 'encounters') {
            if ($this->rise_up_encounters == 1 && $this->rise_up_encounters_exp > time()) {
                return true;
            } else {
                return false;
            }
        } elseif ($type == 'people') {
            if ($this->rise_up_people == 1 && $this->rise_up_people_exp > time()) {
                return true;
            } else {
                return false;
            }
        } elseif ($type == 'spotlight') {
            if ($this->is_featured == 1 && $this->is_featured_exp > time()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function hasVerification($type) {
        $query = $this->db->query("SELECT id FROM verifications WHERE type='" . $type . "' AND user_id='" . $this->id . "'");
        if ($query->num_rows >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function unlockText($trigger, $content, $profile_id) {
        if ($this->id == $profile_id) {
            return $content;
        } else {
            if ($trigger == 'work_and_education') {
                if (empty($this->work) || empty($this->education)) {
                    return substr($content, 0, 6) . '<span class="full-text">' . substr($content, 6) . '</span>
					<span class="show-text" data-toggle="modal" data-target="#unlock-education">Show all</span>';
                } else {
                    return $content;
                }
            } else {
                return $content;
            }
        }
    }

    public function convertHereTo() {
        $here_to = $this->here_to;
        switch ($here_to) {
            case 1:
                return '' . $this->translate('Here_Friends') . '';
                break;
            case 2:
                return '' . $this->translate('Here_Chat') . '';
                break;
            case 3:
                return '' . $this->translate('Here_Date') . '';
                break;
            default:
                return '' . $this->translate('Here_Friends') . '';
                break;
        }
    }

    public function convertAge() {
        $starttime = $this->birthday;
        $endtime = time();
        $age = date('Y', $endtime) - date('Y', $starttime);
        if (date('z', $endtime) < date('z', $starttime)) {
            $age--;
        }
        return $age;
    }

    public function NotificationCount($notification) {
        if ($notification == 'messages') {
            $notifications = $this->db->query("
				SELECT * FROM messages 
				WHERE (user1='" . $this->id . "' OR user2='" . $this->id . "') 
				AND (is_seen_1='0' OR is_seen_2='0')");
            if ($notifications->num_rows > 0) {
                $count = 0;
                while ($notification = $notifications->fetch_object()) {
                    if ($this->id == $notifications->user1) {
                        if ($notification->is_seen_1 == 0) {
                            $count++;
                        }
                    } elseif ($this->id == $notification->user2) {
                        if ($notification->is_seen_2 == 0) {
                            $count++;
                        }
                    }
                }
            } else {
                $count = 0;
            }
        } else {
            $notifications = $this->db->query("SELECT id FROM " . $notification . " WHERE profile_id='" . $this->id . "' AND is_seen='0'");
            $count = $notifications->num_rows;
        }
        if ($count > 0) {
            if (strlen($count) == 1) {
                return '
				<span class="notifications animated infinite flash badge"> 
				<p class="one-digit">' . $count . '</p> 
				</span>';
            } elseif (strlen($count) == 2) {
                return '
				<span class="notifications animated infinite flash badge"> 
				<p class="two-digit">' . $count . '</p>
				</span>';
            }
        }
    }

    public function markAsSeen($notification, $id) {
        $this->db->query("UPDATE " . $notification . " SET is_seen='1' WHERE id='" . $id . "'");
    }

    public function checkIfSeen($notification, $id) {
        $query = $this->db->query("SELECT id FROM " . $notification . " WHERE id='" . $id . "' AND is_seen='1'");
        if ($query->num_rows >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getScore() {
        $likes = $this->db->query("SELECT id FROM profile_likes WHERE profile_id='" . $this->id . "'");
        $likes = $likes->num_rows;
        $dislikes = $this->db->query("SELECT id FROM profile_dislikes WHERE profile_id='" . $this->id . "'");
        $dislikes = $dislikes->num_rows;
        $total = ($likes + $dislikes);
        $percentage = ($likes / $total);
        $result = array('likes' => $likes, 'dislikes' => $dislikes, 'total' => $total, 'percentage' => $percentage);
        $display_value = number_format($percentage * 10, 1);
        switch (strlen($display_value)) {
            case 2:
                $result['class'] = 'one-digit';
                break;
            case 3:
                $result['class'] = 'two-digit';
                break;
            case 4:
                $result['class'] = 'three-digit';
                break;
        }
        $result['display_value'] = number_format($percentage * 10, 1);
        if (strtolower($display_value) == 'nan') {
            $result['display_value'] = '0.0';
            $result['class'] = 'two-digit';
        }
        return $result;
    }

    public function getGenderInterest() {
        if ($this->sexuality == 'Straight' && $this->gender == 'Male') {
            return '' . $this->translate('Girls') . '';
        } elseif ($this->sexuality == 'Gay' && $this->gender == 'Male') {
            return '' . $this->translate('Boys') . '';
        } elseif ($this->sexuality == 'Straight' && $this->gender == 'Female') {
            return '' . $this->translate('Boys') . '';
        } elseif ($this->sexuality == 'Lesbian' && $this->gender == 'Female') {
            return '' . $this->translate('Girls') . '';
        } else {
            return '' . $this->translate('People') . '';
        }
    }

    public function getDistance($user2, $format = true) {
        if ($this->country == $user2->country) {
            $distance = $this->coordsToKm($this->latitude, $this->longitude, $user2->latitude, $user2->longitude);
            if ($distance > 0) {
                if ($format == true) {
                    $distance = '~' . sprintf($this->translate('km_away'), ceil($distance)) . '';
                } else {
                    $distance = ceil($distance);
                }
            } else {
                $distance = $user2->city;
            }
        } else {
            $distance = $user2->city;
        }
        return $distance;
    }

    public function getPhotos($limit = false) {
        if (!is_numeric($limit)) {
            $limit = '';
        } else {
            $limit = 'LIMIT ' . $limit;
        }
        $photos = $this->db->query("
			SELECT * FROM uploaded_photos 
			WHERE user_id=" . $this->id . " " . $limit . "");
        return $photos;
    }

    public function getCommonInterests($user2) {
        $common = $this->db->query("
			SELECT * FROM interests 
			AS user1 JOIN interests 
			AS user2 
			WHERE user1.name = user2.name 
			AND (user1.user_id='" . $this->id . "' 
				AND user2.user_id='" . $user2 . "') 
		GROUP BY user1.name");
        return $common->num_rows;
    }

    public function getInterests() {
        $interests = $this->db->query("
			SELECT * FROM interests 
			WHERE user_id=" . $this->id . "
			GROUP BY name 
			ORDER BY id ASC");
        return $interests;
    }

    public function getVerifications() {
        $verifications = $this->db->query("
			SELECT * FROM verifications 
			WHERE user_id=" . $this->id . "
			AND is_active='1'");
        return $verifications;
    }

    public function getAwards() {
        $awards = $this->db->query("
			SELECT * FROM owned_awards 
			WHERE user_id=" . $this->id . "");
        return $awards;
    }

    public function getGifts() {
        $gifts = $this->db->query("
			SELECT * FROM gifts 
			WHERE user2=" . $this->id . "");
        return $gifts;
    }

    public function getAward($award_id) {
        $award = $this->db->query("
			SELECT * FROM awards
			WHERE id=" . $award_id . "");
        $award = $award->fetch_object();
        return $award;
    }

    public function canLike($profile_id) {
        $check = $this->db->query("
			SELECT id FROM profile_likes 
			WHERE viewer_id=" . $this->id . "
			AND profile_id=" . $profile_id . "");
        if ($check->num_rows == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function canDislike($profile_id) {
        $check = $this->db->query("
			SELECT id FROM profile_dislikes 
			WHERE viewer_id=" . $this->id . " 
			AND profile_id=$profile_id");
        if ($check->num_rows == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function canFavorite($profile_id) {
        $check = $this->db->query("
			SELECT id FROM profile_favorites 
			WHERE viewer_id=" . $this->id . "
			AND profile_id=" . $profile_id . "");
        if ($check->num_rows == 0) {
            return true;
        } else {
            return false;
        }
    }

}
